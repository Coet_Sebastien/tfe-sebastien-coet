using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using TFE_Project.Models;
using Microsoft.AspNetCore.Builder;
using System;
using System.Threading.Tasks;

namespace TFE_Project
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            string connectionString = Configuration.GetConnectionString("tfe-mysql");
            services.AddDbContext<TFEContext>(options =>
            {
                options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString));
                options.UseLazyLoadingProxies();
            });
            services.AddMvc().AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            // configure jwt authentication

            //------------------------------

            // Notre cl� secr�te pour les jetons sur le back-end

            var key = Encoding.ASCII.GetBytes("my-super-secret-key");

            // On pr�cise qu'on veut travaille avec JWT tant pour l'authentification 

            // que pour la v�rification de l'authentification

            services.AddAuthentication(x =>
            {

                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;

                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            })

                .AddJwtBearer(x =>
                {

                    // On exige des requ�tes s�curis�es avec HTTPS

                    x.RequireHttpsMetadata = true;

                    x.SaveToken = true;

                    // On pr�cise comment un jeton re�u doit �tre valid�

                    x.TokenValidationParameters = new TokenValidationParameters
                    {

                        // On v�rifie qu'il a bien �t� sign� avec la cl� d�finie ci-dessous

                        ValidateIssuerSigningKey = true,

                        IssuerSigningKey = new SymmetricSecurityKey(key),

                        // On ne v�rifie pas l'identit� de l'�metteur du jeton

                        ValidateIssuer = false,

                        // On ne v�rifie pas non plus l'identit� du destinataire du jeton

                        ValidateAudience = false,

                        // Par contre, on v�rifie la validit� temporelle du jeton

                        ValidateLifetime = false,

                        // On pr�cise qu'on n'applique aucune tol�rance de validit� temporelle

                        ClockSkew = TimeSpan.Zero  //the default for this setting is 5 minutes

                    };

                    // On peut d�finir des �v�nements li�s � l'utilisation des jetons

                    x.Events = new JwtBearerEvents
                    {

                        // Si l'authentification du jeton est rejet�e ...

                        OnAuthenticationFailed = context =>

                        {

                            // ... parce que le jeton est expir� ...

                            if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                            {

                                // ... on ajoute un header � destination du front-end indiquant cette expiration

                                context.Response.Headers.Add("Token-Expired", "true");

                            }

                            return Task.CompletedTask;

                        }

                    };

                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseRouting();
            app.UseStaticFiles();
            app.UseAuthorization();
            app.UseHttpsRedirection();




            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
               name: "default",
               pattern: "{controller}/{action=Index}/{id?}");

                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}