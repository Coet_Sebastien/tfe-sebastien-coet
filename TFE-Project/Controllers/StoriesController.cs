﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TFE_Project.Models;
using TFE_Project.Framework;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Diagnostics.Metrics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using TFE_Project.Helpers;

namespace TFE_Project.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StoriesController : ControllerBase
    {
        private readonly TFEContext _context;

        public StoriesController(TFEContext context)
        {
            _context = context;
        }

        // GET: api/Stories
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StoryDTO>>> GetStories()
        {
            if (_context.Stories == null)
            {
                return NotFound();
            }
            return (await _context.Stories.ToListAsync()).ToDTO();
        }


        // GET: api/Stories/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<StoryDTO>> GetStory(int id)
        {
            if (_context.Stories == null)
            {
                return NotFound();
            }
            var story = await _context.Stories.FindAsync(id);

            if (story == null)
            {
                return NotFound();
            }

            return story.ToDTO();
        }

        [HttpGet("from/{id}")]
        public async Task<ActionResult<IEnumerable<StoryDTO>>> GetAllStoriesForUser(int id)
        {
            //return _context.Stories.Where(p => p.Author.Id == userId).ToDTO();
            var stories = _context.Stories;
            var test = new List<Story>();
            foreach (var story in stories)
            {
                if (story.AuthorId == id)
                    test.Add(story);
            }
            if (test.Count == 0)
                return NotFound();
            Story[] res = test.ToArray();
            return res.ToDTO();
        }

        // PUT: api/Stories/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStory(int id, StoryDTO storyDTO)
        {
            //for a clean put, should move the players on soon-to-be deleted chapters via request on api/players
            //new chapters should have a negative id like 'id:-5'
            if (id != storyDTO.StoryId)
            {
                return BadRequest();
            }
            var story = await _context.Stories.FindAsync(id);
            if (story == null)
                return NotFound();
            story.Summary = storyDTO.Summary;
            story.Title = storyDTO.Title;
            //delete outdated chapters
            //list chapter'ids from DTO
            var chapterIdFromDTO = storyDTO.Chapters.Where(c => c.Id > 0 && c.StoryId == story.StoryId).Select(c => c.Id);
            foreach (var chapter in story.Chapters)
            {
                //if the chapter isn't present in the list, remove
                if (!chapterIdFromDTO.Contains(chapter.Id))
                {
                    _context.Remove(chapter);
                }
                //else update
                else
                {
                    // var tmp = storyDTO.Chapters.Where(c => c.Id == chapter.Id).SingleOrDefault();
                    // if (tmp != null)
                    // {
                    //     chapter.Body = tmp.Body;
                    //     chapter.X=tmp.X;
                    //     chapter.Y=tmp.Y;
                    // }
                    switch (chapter.Type)
                    {
                        case "Combat":
                            Combat combat = (Combat)chapter;
                            var tmp = storyDTO.Chapters.Where(c => c.Id == chapter.Id).SingleOrDefault();
                            if (tmp != null)
                            {
                                if (tmp.VictoryId == null
                                || tmp.Ability == null
                                || tmp.Endurance == null)
                                {
                                    return Problem("Chapter n°" + tmp.Id + " of type Combat is incomplete");
                                }
                                else
                                {
                                    //common
                                    combat.X = tmp.X;
                                    combat.Y = tmp.Y;
                                    //specific
                                    combat.Ability = (int)tmp.Ability;
                                    combat.Endurance = (int)tmp.Endurance;
                                    combat.VictoryId = (int)tmp.VictoryId;
                                }
                            }
                            break;
                        case "EnduranceChangeEvent":
                            EnduranceChangeEvent enduranceChangeEvent = (EnduranceChangeEvent)chapter;
                            var tmp3 = storyDTO.Chapters.Where(c => c.Id == chapter.Id).SingleOrDefault();
                            if (tmp3 != null)
                            {
                                if (tmp3.VictoryId == null
                                || tmp3.EnduranceModifier == null)
                                {
                                    return Problem("Chapter n°" + tmp3.Id + " of type EnduranceChangeEvent is incomplete");
                                }
                                else
                                {
                                    //common
                                    enduranceChangeEvent.X = tmp3.X;
                                    enduranceChangeEvent.Y = tmp3.Y;
                                    //specific
                                    enduranceChangeEvent.EnduranceModifier = (int)tmp3.EnduranceModifier;
                                    enduranceChangeEvent.VictoryId = (int)tmp3.VictoryId;
                                }
                            }
                            break;
                        case "Scenario":
                            Scenario scenario = (Scenario)chapter;
                            var tmp2 = storyDTO.Chapters.Where(c => c.Id == chapter.Id).SingleOrDefault();
                            if (tmp2 != null)
                            {
                                if (tmp2.Body == null)
                                {
                                    return Problem("Chapter n°" + tmp2.Id + " of type Scenario is incomplete");
                                }
                                else
                                {
                                    //common
                                    scenario.X = tmp2.X;
                                    scenario.Y = tmp2.Y;
                                    //specific
                                    scenario.Body = tmp2.Body;
                                }
                            }
                            break;
                    }
                }
            }
            await _context.SaveChangesAsync();
            //add new chapters
            var chapterIdFromDTOToAdd = storyDTO.Chapters.Where(c => c.Id < 0 && c.StoryId == story.StoryId);
            var savedNewChapters = new List<SavedNewChapter>();
            foreach (var chapter in chapterIdFromDTOToAdd)
            {
                // var tmp = new Chapter
                // {
                //     Body = chapter.Body,
                //     StoryId = story.StoryId,
                //     X=chapter.X,
                //     Y=chapter.Y,
                //     IncomingRelationships = new List<Relationship>(),
                //     OutgoingRelationships = new List<Relationship>(),
                //     CurrentPlayers = new List<Player>()
                // };
                // _context.Chapters.Add(tmp);
                // await _context.SaveChangesAsync();
                // savedNewChapters.Add(new SavedNewChapter { OldId = chapter.Id, IdNewChapter = tmp.Id });
                switch (chapter.Type)
                {
                    case "Combat":
                        if (chapter.VictoryId == null
                                || chapter.Ability == null
                                || chapter.Endurance == null)
                        {
                            return Problem("Chapter n°" + chapter.Id + " of type Combat is incomplete");
                        }
                        else
                        {
                            var tmpCombat = new Combat
                            {
                                Type = "Combat",
                                StoryId = story.StoryId,
                                X = chapter.X,
                                Y = chapter.Y,
                                IncomingRelationships = new List<Relationship>(),
                                OutgoingRelationships = new List<Relationship>(),
                                CurrentPlayers = new List<Player>(),
                                Story = story,
                                //specific
                                Endurance = (int)chapter.Endurance,
                                Ability = (int)chapter.Ability,
                                VictoryId = (int)chapter.VictoryId
                            };
                            _context.Chapters.Add(tmpCombat);
                            await _context.SaveChangesAsync();
                            savedNewChapters.Add(new SavedNewChapter { OldId = chapter.Id, IdNewChapter = tmpCombat.Id });
                        }
                        break;
                    case "EnduranceChangeEvent":
                        if (chapter.VictoryId == null
                            || chapter.EnduranceModifier == null)
                        {
                            return Problem("Chapter n°" + chapter.Id + " of type EnduranceChangeEvent is incomplete");
                        }
                        else
                        {
                            var tmpEnduranceChangeEvent = new EnduranceChangeEvent
                            {
                                Type = "EnduranceChangeEvent",
                                StoryId = story.StoryId,
                                X = chapter.X,
                                Y = chapter.Y,
                                IncomingRelationships = new List<Relationship>(),
                                OutgoingRelationships = new List<Relationship>(),
                                CurrentPlayers = new List<Player>(),
                                Story = story,
                                //specific
                                EnduranceModifier = (int)chapter.EnduranceModifier,
                                VictoryId = (int)chapter.VictoryId
                            };
                            _context.Chapters.Add(tmpEnduranceChangeEvent);
                            await _context.SaveChangesAsync();
                            savedNewChapters.Add(new SavedNewChapter { OldId = chapter.Id, IdNewChapter = tmpEnduranceChangeEvent.Id });
                        }
                        break;
                    case "Scenario":
                        if (chapter.Body == null)
                        {
                            return Problem("Chapter n°" + chapter.Id + " of type Scenario is incomplete");
                        }
                        else
                        {
                            var tmpScenario = new Scenario
                            {
                                Type = "Scenario",
                                StoryId = story.StoryId,
                                X = chapter.X,
                                Y = chapter.Y,
                                IncomingRelationships = new List<Relationship>(),
                                OutgoingRelationships = new List<Relationship>(),
                                CurrentPlayers = new List<Player>(),
                                Story = story,
                                //specific
                                Body = chapter.Body
                            };
                            _context.Chapters.Add(tmpScenario);
                            await _context.SaveChangesAsync();
                            savedNewChapters.Add(new SavedNewChapter { OldId = chapter.Id, IdNewChapter = tmpScenario.Id });
                        }
                        break;
                }
            }
            await _context.SaveChangesAsync();
            //delete outdated relationships
            //list of relationships in DTO WITH existing Chapters
            var relationshipsFromDTO = storyDTO.Relationships.Where(r => r.Input > 0 && r.Output > 0);
            var chapterIdsFromStory = story.Chapters.Select(c => c.Id);
            foreach (var relationship in story.Relationships)
            {
                bool relationshipAlreadyExistsInStory = false;
                foreach (var item in relationshipsFromDTO)
                {
                    if (item.Input == relationship.Input && item.Output == relationship.Output)
                    {
                        relationshipAlreadyExistsInStory = true;
                        relationship.Body = item.Body;
                    }
                }
                if (!relationshipAlreadyExistsInStory)
                {
                    _context.Remove(relationship);
                }
            }
            await _context.SaveChangesAsync();

            foreach (var item in relationshipsFromDTO)
            {
                bool alreadyExists = false;
                foreach (var relationship in story.Relationships)
                {
                    if (item.Input == relationship.Input && item.Output == relationship.Output)
                    {
                        alreadyExists = true;
                    }
                }
                if (!alreadyExists)
                {
                    if (chapterIdsFromStory.Contains(item.Input) && chapterIdsFromStory.Contains(item.Output))
                    {
                        //add new Relationships using existing Chapters
                        var tmp = new Relationship
                        {
                            Input = item.Input,
                            Origin = await _context.Chapters.FindAsync(item.Input),
                            Output = item.Output,
                            Destination = await _context.Chapters.FindAsync(item.Output),
                            StoryId = story.StoryId,
                            Body = item.Body
                        };
                        _context.Relationships.Add(tmp);
                    }
                }
            }
            await _context.SaveChangesAsync();
            //add relationships
            //list of relationships in DTO WITH one of the Chapters newly added
            var relationshipsFromDTOToAdd = storyDTO.Relationships.Where(r => r.Input < 0 || r.Output < 0);
            foreach (var relationship in relationshipsFromDTOToAdd)
            {
                var tmp = new Relationship()
                {
                    StoryId = story.StoryId,
                    Body = relationship.Body
                };
                var checkInput = savedNewChapters.Where(snc => snc.OldId == relationship.Input).SingleOrDefault();
                var checkOutput = savedNewChapters.Where(snc => snc.OldId == relationship.Output).SingleOrDefault();
                if (checkInput != null)
                {
                    tmp.Input = checkInput.IdNewChapter;
                    tmp.Origin = await _context.Chapters.FindAsync(checkInput.IdNewChapter);
                }
                else
                {
                    tmp.Input = relationship.Input;
                    tmp.Origin = await _context.Chapters.FindAsync(relationship.Input);
                }
                if (checkOutput != null)
                {
                    tmp.Output = checkOutput.IdNewChapter;
                    tmp.Destination = await _context.Chapters.FindAsync(checkOutput.IdNewChapter);
                }
                else
                {
                    tmp.Output = relationship.Output;
                    tmp.Destination = await _context.Chapters.FindAsync(relationship.Output);
                }
                _context.Relationships.Add(tmp);
            }
            //convert VictoryId
            foreach (var chapter in story.Chapters)
            {
                if (chapter.Type == "Combat")
                {
                    Combat combat = (Combat)chapter;
                    if (combat.VictoryId < 0)
                    {
                        var checkID = savedNewChapters.Where(snc => snc.OldId == combat.VictoryId).SingleOrDefault();
                        if (checkID != null)
                        {
                            combat.VictoryId = checkID.IdNewChapter;
                        }
                    }
                }
                if (chapter.Type == "EnduranceChangeEvent")
                {
                    EnduranceChangeEvent enduranceChangeEvent = (EnduranceChangeEvent)chapter;
                    if (enduranceChangeEvent.VictoryId < 0)
                    {
                        var checkID = savedNewChapters.Where(snc => snc.OldId == enduranceChangeEvent.VictoryId).SingleOrDefault();
                        if (checkID != null)
                        {
                            enduranceChangeEvent.VictoryId = checkID.IdNewChapter;
                        }
                    }
                }
            }
            await _context.SaveChangesAsync();
            var res = await _context.SaveChangesAsyncWithValidation();
            if (!res.IsEmpty)
                return BadRequest(res);
            return CreatedAtAction(nameof(GetStory), new { id = story.StoryId }, story.ToDTO());
        }

        private class SavedNewChapter
        {
            public int OldId { get; set; }
            public int IdNewChapter { get; set; }
        }

        // POST: api/Stories
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<StoryDTO>> PostStory(StoryDTO storyDTO)
        {
            if (_context.Stories == null)
            {
                return Problem("Entity set 'TFEContext.Stories'  is null.");
            }
            var story = new Story
            {
                AuthorId = storyDTO.Author.Id,
                Summary = storyDTO.Summary,
                Title = storyDTO.Title,
                Author = await _context.Users.FindAsync(storyDTO.Author.Id)
            };
            _context.Stories.Add(story);
            await _context.SaveChangesAsync();
            //ajout automatique d'un chapitre de départ
            int idNewStory = story.StoryId;
            var BeginningChapter = new Scenario
            {
                Body = "New",
                Story = await _context.Stories.FindAsync(idNewStory),
                X = 0,
                Y = 0
            };
            _context.Chapters.Add(BeginningChapter);
            await _context.SaveChangesAsync();
            var BeginningChapterId = BeginningChapter.Id;
            story.BeginningChapterId = BeginningChapterId;
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetStory), new { id = story.StoryId });
        }

        // DELETE: api/Stories/5/1
        [HttpDelete("{id}/{userId}")]
        public async Task<IActionResult> DeleteStory(int id, int userId)
        {
            if (_context.Stories == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(userId);
            if (user == null)
            {
                return NotFound();
            }
            var story = await _context.Stories.FindAsync(id);
            if (story == null)
            {
                return NotFound();
            }
            if (user.Role == Role.Member && story.AuthorId == userId)
            {
                _context.Stories.Remove(story);
                await _context.SaveChangesAsync();
            }
            if (user.Role == Role.Admin)
            {
                _context.Stories.Remove(story);
                await _context.SaveChangesAsync();
            }

            return NoContent();
        }

        private bool StoryExists(int id)
        {
            return (_context.Stories?.Any(e => e.StoryId == id)).GetValueOrDefault();
        }

        // GET: api/Stories/5
        [AllowAnonymous]
        [HttpGet("stats/{id}")]
        public async Task<ActionResult<StatDTO>> GetStatsForStory(int id)
        {
            if (_context.Stories == null)
            {
                return NotFound();
            }
            var story = await _context.Stories.FindAsync(id);
            if (story == null)
            {
                return NotFound();
            }
            var stats = await _context.PlayerLogs.Where(pl => pl.StoryId == id).ToListAsync();
            var statsToSend = new StatDTO();
            foreach (var chapter in story.Chapters)
            {
                var statToAdd = new StatChapterDTO
                {
                    ChapterId=chapter.Id
                };
                statToAdd.TotalPlayers = stats.Where(pl => pl.ChapterId==chapter.Id && pl.Action!="DIED" && pl.Action!="QUIT" && pl.Action!="END").Count();
                statToAdd.Quits = stats.Where(pl => pl.ChapterId==chapter.Id && (pl.Action=="QUIT" || pl.Action=="END")).Count();
                statToAdd.Deaths = stats.Where(pl => pl.ChapterId==chapter.Id && pl.Action=="DIED").Count();
                statToAdd.NbCurrentPlayers = chapter.CurrentPlayers.Count();
                statsToSend.StatChaptersDTO.Add(statToAdd);
            }
            foreach (var rel in story.Relationships)
            {
                var statToAdd = new StatRelDTO
                {
                    Input=rel.Input,
                    Output=rel.Output,
                    TotalPlayers=stats.Where(pl => pl.OldChapterId==rel.Input && pl.ChapterId==rel.Output).Count()
                };
                statsToSend.StatRelsDTO.Add(statToAdd);
            }
            return statsToSend;
        }
    }
}
