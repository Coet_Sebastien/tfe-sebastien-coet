using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TFE_Project.Models;
using TFE_Project.Framework;
using static System.Runtime.InteropServices.JavaScript.JSType;
using NuGet.Packaging;
using System.Diagnostics.Metrics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using TFE_Project.Helpers;

namespace TFE_Project.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly TFEContext _context;

        public PlayersController(TFEContext context)
        {
            _context = context;
        }

        // GET: api/Players
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerDTO>>> GetPlayers()
        {
            if (_context.Players == null)
            {
                return NotFound();
            }
            return (await _context.Players.ToListAsync()).ToDTO();
        }

        // GET: api/Players/5
        [HttpGet("{discordId}")]
        public async Task<ActionResult<PlayerDTO>> GetPlayer(string discordId)
        {
            //Console.WriteLine("GET ONE");
            if (_context.Players == null || _context.Players.Count() < 1)
            {
                return NotFound();
            }
            var player = await _context.Players.SingleAsync(p => p.DiscordId == discordId);

            if (player == null)
            {
                return NotFound();
            }

            return player.ToDTO();
        }

        // PUT: api/Players/565464313
        [Authorized(Role.Admin)]
        [HttpPut("{discordId}")]
        public async Task<ActionResult<PlayerDTO>> PutPlayer(string discordId, PlayerDTO playerDTO)
        {
            //Console.WriteLine("PUT");
            if (discordId != playerDTO.DiscordId)
            {
                return BadRequest();
            }
            var player = await _context.Players.SingleAsync(p => p.DiscordId == discordId);
            if (player == null)
                return NotFound();
            var checkOutputs = player.CurrentChapter.OutgoingRelationships.Where(r => r.Output == playerDTO.ChapterId);
            if (checkOutputs.Count() > 0)
            {
                var saveId = player.ChapterId;
                var typeOld= player.CurrentChapter.Type;
                player.CurrentChapter = await _context.Chapters.FindAsync(playerDTO.ChapterId);
                player.ChapterId = playerDTO.ChapterId;
                if (playerDTO.Endurance != 0)
                {
                    player.Endurance = playerDTO.Endurance;
                }
                if (_context.PlayerLogs != null)
                {
                    //logging
                    var playerLog = new PlayerLog()
                    {
                        DiscordId = player.DiscordId,
                        ChapterId = player.ChapterId,
                        OldChapterId = saveId,
                        StoryId = player.CurrentChapter.StoryId,
                        EnduranceLeft = player.Endurance,
                        Action = "MOVE FROM "+typeOld
                    };
                    _context.PlayerLogs.Add(playerLog);
                }
                var res = await _context.SaveChangesAsyncWithValidation();
                if (!res.IsEmpty)
                    return BadRequest(res);
                return player.ToDTO();
            }
            return Problem("Wrong chapter");

        }


        // To move the player without Endurance change
        // PUT: api/Players/565464313/1
        [Authorized(Role.Admin)]
        [HttpPut("movePlayer/{discordId}/{choice}")]
        public async Task<ActionResult<PlayerDTO>> MovePlayer(string discordId, int choice)
        {
            var player = await _context.Players.SingleAsync(p => p.DiscordId == discordId);
            if (player == null)
                return NotFound();
            if (player.CurrentChapter.OutgoingRelationships.Count > choice-1 && choice>0)
            {

                var saveId = player.ChapterId;
                var typeOld = player.CurrentChapter.Type;
                var outgoingRel = player.CurrentChapter.OutgoingRelationships.ElementAt(choice-1);
                player.ChapterId = outgoingRel.Output;
                player.CurrentChapter = await _context.Chapters.FindAsync(outgoingRel.Output);
                if (_context.PlayerLogs != null)
                {
                    //logging
                    var playerLog = new PlayerLog()
                    {
                        DiscordId = player.DiscordId,
                        ChapterId = player.ChapterId,
                        OldChapterId = saveId,
                        StoryId = player.CurrentChapter.StoryId,
                        EnduranceLeft = player.Endurance,
                        Action = "MOVE FROM "+typeOld
                    };
                    _context.PlayerLogs.Add(playerLog);
                }
                var res = await _context.SaveChangesAsyncWithValidation();
                if (!res.IsEmpty)
                    return BadRequest(res);
                return player.ToDTO();
            }
            return Problem("Wrong chapter");

        }

        // POST: api/Players
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorized(Role.Admin)]
        [HttpPost]
        public async Task<ActionResult<PlayerDTO>> PostPlayer(PlayerDTO playerDTO)
        {
            if (_context.Players == null)
            {
                return Problem("Entity set 'TFEContext.Players'  is null.");
            }
            var chapter = await _context.Chapters.FindAsync(playerDTO.ChapterId);
            var playerTest = await _context.Players.SingleOrDefaultAsync(p => p.DiscordId == playerDTO.DiscordId);
            if (chapter != null && playerTest == null)
            {
                var story = chapter.Story;
                story.TotalPlayers+=1;
                var player = new Player()
                {
                    DiscordId = playerDTO.DiscordId,
                    ChapterId = playerDTO.ChapterId,
                    CurrentChapter = chapter,
                    Endurance = 25,
                    Ability = 15
                };
                _context.Players.Add(player);
                if (_context.PlayerLogs != null)
                {
                    //logging
                    var playerLog = new PlayerLog()
                    {
                        DiscordId = player.DiscordId,
                        ChapterId = player.ChapterId,
                        StoryId = chapter.StoryId,
                        EnduranceLeft = player.Endurance,
                        Action = "CREATED"
                    };
                    _context.PlayerLogs.Add(playerLog);
                }
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetPlayer", new { id = player.Id }, player.ToDTO());
            }
            else
            {
                return Problem("Chapter selected is invalid or save already exists");
            }

        }

        // DELETE: api/Players/5
        [Authorized(Role.Admin)]
        [HttpDelete("{discordId}")]
        public async Task<IActionResult> DeletePlayer(string discordId)
        {
            if (_context.Players == null)
            {
                return NotFound();
            }
            var player = await _context.Players.SingleOrDefaultAsync(p => p.DiscordId == discordId);
            if (player == null)
            {
                return NotFound();
            }
            if (_context.PlayerLogs != null)
            {
                var typeOld= player.CurrentChapter.Type;
                //logging
                var playerLog = new PlayerLog()
                {
                    DiscordId = discordId,
                    ChapterId = player.ChapterId,
                    StoryId = player.CurrentChapter.StoryId,
                    EnduranceLeft = player.Endurance,
                };
                if(typeOld=="Scenario"){
                    if(player.CurrentChapter.OutgoingRelationships.Count()<1){
                        playerLog.Action = "END";
                    }else{
                        playerLog.Action = "QUIT";
                    }
                    
                }else{
                    playerLog.Action = "DIED";
                }
                _context.PlayerLogs.Add(playerLog);
                await _context.SaveChangesAsync();
            }
            _context.Players.Remove(player);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PlayerExists(int id)
        {
            return (_context.Players?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        [HttpPut("movePlayers/{idOldChapter}/{idTargetChapter}")]
        public async Task<IActionResult> MovePlayers(int idOldChapter, int idTargetChapter)
        {
            if (_context.Chapters == null)
            {
                return NotFound();
            }
            var oldChapter = await _context.Chapters.FindAsync(idOldChapter);
            var targetChapter = await _context.Chapters.FindAsync(idTargetChapter);
            if (oldChapter == null || targetChapter == null || oldChapter.StoryId != targetChapter.StoryId)
            {
                return NotFound();
            }
            targetChapter.CurrentPlayers.AddRange(oldChapter.CurrentPlayers);
            var res = await _context.SaveChangesAsyncWithValidation();
            if (!res.IsEmpty)
                return BadRequest(res);
            return NoContent();
        }
    }
}
