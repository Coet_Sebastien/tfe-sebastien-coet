// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.AspNetCore.Http;
// using Microsoft.AspNetCore.Mvc;
// using Microsoft.EntityFrameworkCore;
// using TFE_Project.Models;
// using TFE_Framework;
// using NuGet.Packaging;
// using static System.Runtime.InteropServices.JavaScript.JSType;
// using System.Diagnostics.Metrics;
// using Microsoft.AspNetCore.Authorization;
// using Microsoft.Extensions.Hosting;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Microsoft.IdentityModel.Tokens;
// using System.IdentityModel.Tokens.Jwt;
// using Microsoft.AspNetCore.Http;
// using System.IO;
// using System.Text;
// using System.Security.Claims;

// namespace TFE_Project.Controllers
// {
//     [Route("api/[controller]")]
//     [ApiController]
//     public class ChaptersController : ControllerBase
//     {
//         private readonly TFEContext _context;

//         public ChaptersController(TFEContext context)
//         {
//             _context = context;
//         }

//         // GET: api/Chapters
//         [HttpGet]
//         public async Task<ActionResult<IEnumerable<ChapterDTO>>> GetChapters()
//         {
//             if (_context.Chapters == null)
//             {
//                 return NotFound();
//             }
//             return (await _context.Chapters.ToListAsync()).ToDTO();
//         }

//         // GET: api/Chapters/5
//         [HttpGet("{id}")]
//         public async Task<ActionResult<ChapterDTO>> GetChapter(int id)
//         {
//             if (_context.Chapters == null)
//             {
//                 return NotFound();
//             }
//             var chapter = await _context.Chapters.FindAsync(id);

//             if (chapter == null)
//             {
//                 return NotFound();
//             }

//             return chapter.ToDTO();
//         }

//         // PUT: api/Chapters/5
//         // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//         [HttpPut("{id}")]
//         public async Task<IActionResult> PutChapter(int id, ChapterDTO chapterDTO)
//         {
//             if (id != chapterDTO.Id)
//             {
//                 return BadRequest();
//             }
//             // var chapter = await _context.Chapters.FindAsync(id);
//             // if (chapter == null)
//             //     return NotFound();
//             // chapter.X=chapterDTO.X;
//             // chapter.Y=chapterDTO.Y;
//             switch (chapterDTO.Type)
//             {
//                 case "Combat":
//                     CombatDTO combatDTO=(CombatDTO)chapterDTO;
//                     Combat chapter = (Combat)await _context.Chapters.FindAsync(id);
//                     if (chapter == null || chapter.Type!="Combat")
//                         return NotFound();
//                     chapter.X = combatDTO.X;
//                     chapter.Y = combatDTO.Y;
//                     //specific
//                     chapter.VictoryId=combatDTO.VictoryId;
//                     chapter.Endurance=combatDTO.Endurance;
//                     chapter.Ability=combatDTO.Ability;
//                     break;
//                 case "Scenario":
//                     break;
//                 default:
//                     break;
//             }
//             var res = await _context.SaveChangesAsyncWithValidation();
//             if (!res.IsEmpty)
//                 return BadRequest(res);
//             return NoContent();
//         }

//         // POST: api/Chapters
//         // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//         [HttpPost]
//         public async Task<ActionResult<ChapterDTO>> PostChapter(ChapterDTO chapterDTO)
//         {
//             if (_context.Chapters == null)
//             {
//                 return Problem("Entity set 'TFEContext.Chapters'  is null.");
//             }
//             if (chapterDTO.IncomingRelationships.First() == null)
//             {
//                 return Problem("No origin given");
//             }
//             var story = await _context.Stories.FindAsync(chapterDTO.StoryId);
//             if (story == null)
//             {
//                 return Problem("StoryId is not correct");
//             }
//             var test = await _context.Chapters.FindAsync(chapterDTO.IncomingRelationships.First().Input);
//             if (test == null)
//             {
//                 return Problem("Origin is not correct");
//             }
//             var chapter = new Chapter
//             {
//                 Body= chapterDTO.Body,
//                 StoryId=story.StoryId,
//                 X=chapterDTO.X,
//                 Y=chapterDTO.Y,
//                 IncomingRelationships = new List<Relationship>(),
//                 OutgoingRelationships = new List<Relationship>(),
//                 CurrentPlayers = new List<Player>()
//             };
//             _context.Chapters.Add(chapter);
//             var tmp = new Relationship
//             {
//                 Input = test.Id,
//                 Origin = test,
//                 Output = chapter.Id,
//                 Destination = chapter,
//                 Body = chapterDTO.IncomingRelationships.First().Body,
//                 StoryId = story.StoryId,
//             };
//             _context.Relationships.Add(tmp);
//             await _context.SaveChangesAsync();
//             return CreatedAtAction(nameof(GetChapter), new { id = chapter.Id }, chapter.ToDTO());
//         }

//         // DELETE: api/Chapters/5
//         [HttpDelete("{id}")]
//         public async Task<IActionResult> DeleteChapter(int id)
//         {
//             if (_context.Chapters == null)
//             {
//                 return NotFound();
//             }
//             var chapter = await _context.Chapters.FindAsync(id);
//             //We can't delete the departure
//             if (chapter == null || chapter.IncomingRelationships.Count == 0)
//             {
//                 return NotFound();
//             }
//             //transferring the outgoing relationships to the incoming relationships before removing
//             if (chapter.OutgoingRelationships.Count != 0)
//             {
//                 foreach (var outgoingRelationship in chapter.OutgoingRelationships)
//                 {
//                     int saveOutputId = outgoingRelationship.Output;
//                     foreach (var incomingReltionship in chapter.IncomingRelationships)
//                     {
//                         //verification if the relationship already exists
//                         var test = await _context.Relationships.Where(r => r.Input == incomingReltionship.Input && r.Output == saveOutputId).SingleOrDefaultAsync();
//                         if (test == null)
//                         {
//                             //add the new relationships
//                             var tmp = new Relationship
//                             {
//                                 Input = incomingReltionship.Input,
//                                 Origin = incomingReltionship.Origin,
//                                 Output = saveOutputId,
//                                 Destination = outgoingRelationship.Destination,
//                                 Body = outgoingRelationship.Body,
//                                 StoryId = outgoingRelationship.Destination.StoryId
//                             };
//                             _context.Relationships.Add(tmp);
//                         }
//                     }
//                     //remove old relationships
//                     _context.Relationships.Remove(outgoingRelationship);
//                 }
//             }
//             _context.Chapters.Remove(chapter);
//             await _context.SaveChangesAsync();

//             return NoContent();
//         }

//         // DELETE: api/Chapters/5/2
//         [HttpDelete("{idOldChapter}/{idTargetChapter}")]
//         public async Task<IActionResult> DeleteChapterAndMovePlayers(int idOldChapter, int idTargetChapter)
//         {
//             if (_context.Chapters == null)
//             {
//                 return NotFound();
//             }
//             var oldChapter = await _context.Chapters.FindAsync(idOldChapter);
//             var targetChapter = await _context.Chapters.FindAsync(idTargetChapter);
//             //We can't delete the departure
//             if (oldChapter == null || targetChapter == null || oldChapter.StoryId != targetChapter.StoryId || oldChapter.IncomingRelationships.Count == 0)
//             {
//                 return NotFound();
//             }
//             targetChapter.CurrentPlayers.AddRange(oldChapter.CurrentPlayers);
//             //transferring the outgoing relationships to the incoming relationships before removing
//             if (oldChapter.OutgoingRelationships.Count != 0)
//             {
//                 foreach (var outgoingRelationship in oldChapter.OutgoingRelationships)
//                 {
//                     int saveOutputId = outgoingRelationship.Output;
//                     foreach (var incomingReltionship in oldChapter.IncomingRelationships)
//                     {
//                         //verification if the relationship already exists
//                         var test = await _context.Relationships.Where(r => r.Input == incomingReltionship.Input && r.Output == saveOutputId).SingleOrDefaultAsync();
//                         if (test == null)
//                         {
//                             //add the new relationships
//                             var tmp = new Relationship
//                             {
//                                 Input = incomingReltionship.Input,
//                                 Origin = incomingReltionship.Origin,
//                                 Output = saveOutputId,
//                                 Destination = outgoingRelationship.Destination,
//                                 Body = outgoingRelationship.Body,
//                                 StoryId = outgoingRelationship.Destination.StoryId
//                             };
//                             _context.Relationships.Add(tmp);
//                         }
//                     }
//                     //remove old relationships
//                     _context.Relationships.Remove(outgoingRelationship);
//                 }
//             }
//             _context.Chapters.Remove(oldChapter);
//             var res = await _context.SaveChangesAsyncWithValidation();
//             if (!res.IsEmpty)
//                 return BadRequest(res);
//             return NoContent();
//         }

//         //changeInputFromTo(chapterDTO)
//         //changeOutputFromTo(chapterDTO)

//         private bool ChapterExists(int id)
//         {
//             return (_context.Chapters?.Any(e => e.Id == id)).GetValueOrDefault();
//         }
//     }
// }
