using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TFE_Project.Models;
using TFE_Project.Framework;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Diagnostics.Metrics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Text;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using TFE_Project.Helpers;

namespace TFE_Project.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly TFEContext _context;

        public UsersController(TFEContext context)
        {
            _context = context;
        }

        // GET: api/Users

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDTO>>> GetUsers()
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            return (await _context.Users.ToListAsync()).ToDTO();
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
                return NotFound();
            return user.ToDTO();
        }

        // GET: api/Users/pseudo/ARandomPseudo
        [HttpGet("pseudo/{pseudo}")]
        public async Task<ActionResult<UserDTO>> GetOneByPseudo(string pseudo)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Pseudo == pseudo);
            if (user == null)
                return NotFound();
            return user.ToDTO();
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, UserDTO userDTO)
        {
            if (id != userDTO.Id)
                return BadRequest();

            var user = await _context.Users.FindAsync(id);

            if (user == null)
                return NotFound();

            var userPseudo = await _context.Users.SingleOrDefaultAsync(u => u.Pseudo == userDTO.Pseudo);
            if (userPseudo != null && user.Pseudo != userDTO.Pseudo)
            {
                var err = new ValidationErrors().Add("Pseudo already used.", nameof(userPseudo.Pseudo));
                return BadRequest(err);
            }
            if (userDTO.Pseudo != "")
                user.Pseudo = userDTO.Pseudo;
            if (userDTO.Password != "")
                user.Password = TokenHelper.GetPasswordHash(userDTO.Password);

            var res = await _context.SaveChangesAsyncWithValidation();

            if (!res.IsEmpty)
                return BadRequest(res);

            return NoContent();
        }

        // POST: api/Users
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<UserDTO>> PostUser(UserDTO data)
        {
            var user = await _context.Users.FindAsync(data.Id);
            if (user != null)
            {
                var err = new ValidationErrors().Add("Id already in use", nameof(user.Id));
                return BadRequest(err);
            }

            var userPseudo = await _context.Users.SingleOrDefaultAsync(u => u.Pseudo == data.Pseudo);
            if (userPseudo != null)
            {
                var err = new ValidationErrors().Add("Pseudo already used.", nameof(userPseudo.Pseudo));
                return BadRequest(err);
            }
            var userEmail = await _context.Users.SingleOrDefaultAsync(u => u.Email == data.Email);
            if (userEmail != null)
            {
                var err = new ValidationErrors().Add("Email already used.", nameof(userEmail.Email));
                return BadRequest(err);
            }

            var newUser = new User()
            {
                //Id = data.Id,
                Pseudo = data.Pseudo,
                Password = TokenHelper.GetPasswordHash(data.Password),
                Email = data.Email,
                Role = data.Role
            };

            _context.Users.Add(newUser);
            var res = await _context.SaveChangesAsyncWithValidation();

            if (!res.IsEmpty)
                return BadRequest(res);
            return CreatedAtAction(nameof(GetUser), new { id = newUser.Id }, newUser.ToDTO());

        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => (e.Id == id))).GetValueOrDefault();
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<ActionResult<User>> Authenticate(UserDTO data)
        {
            var user = await Authenticate(data.Email, data.Password);
            if (user == null)
                return BadRequest(new ValidationErrors().Add("User not found", "Pseudo"));

            if (user.Token == null)
                return BadRequest(new ValidationErrors().Add("Incorrect password", "Password"));

            return Ok(user);
        }

        private async Task<User> Authenticate(string email, string password)
        {
            //Console.Write("Trying to connect with: "+email+" "+password);
            //var user = await _context.Users.FindAsync(pseudo);
            IEnumerable<User> users = _context.Users;
            User user = null;
            foreach (var userTest in users)
            {
                if (userTest.Email == email)
                {
                    user = await _context.Users.FindAsync(userTest.Id);
                }
            }

            // return null if user not found
            if (user == null)
                return null;

            if (user.Password == TokenHelper.GetPasswordHash(password))
            {
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("my-super-secret-key");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                                                 {
                                             new Claim(ClaimTypes.Name, user.Email),
                                             new Claim(ClaimTypes.Role, user.Role.ToString())
                                                 }),
                    IssuedAt = DateTime.UtcNow,
                    Expires = DateTime.UtcNow.AddMinutes(10),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                user.Token = tokenHandler.WriteToken(token);

            }

            // remove password before returning
            user.Password = null;
            // user.Votes = null;
            // user.Posts = null;
            // user.Comments = null;
            return user;
        }

    }
}
