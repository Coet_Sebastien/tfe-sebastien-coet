import { useEffect } from "react";
import { User } from "../models/user";

async function checkStatus(response: any) {
  if (response.ok) {
    return response;
  } else {
    if (response.status === 400) {
      await response.json().then((jsonError:any) => {
        console.log(jsonError.errors);
        if (jsonError.errors.Email !== undefined) {
          throw new Error(jsonError.errors.Email[0]);
        }
        if (jsonError.errors.Pseudo !== undefined) {
          throw new Error(jsonError.errors.Pseudo[0]);
        }
        if (jsonError.errors.Password !== undefined) {
          throw new Error(jsonError.errors.Password[0]);
        }
      });
    }
    throw new Error("An error occurred");
  }
}

function parseJSON(response: Response) {
  return response.json();
}

function convertToUserModel(item: any): User {
  var tmp = new User(item);
  return tmp;
}

// function saveUserInStorage(user: any) {
//   sessionStorage.user=JSON.stringify(user);
//   console.log(user);
//   console.log(sessionStorage.user);
// }

export async function loginUser(email: string, password: string) {
  return await fetch(`/api/users/authenticate`, {
    method: 'POST',
    body: JSON.stringify({ email, password, pseudo: "nnn" }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(checkStatus)
    .then(parseJSON)
    .then(convertToUserModel)
    .catch((error: TypeError) => {
      throw new Error(error.message);;
    });
}

export async function postUser(email: string, password: string, pseudo: string) {
  return await fetch(`/api/users`, {
    method: 'POST',
    body: JSON.stringify({ email, password, pseudo }),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(checkStatus)
    .catch((error: TypeError) => {
      throw new Error(error.message);
    });
}

// export async function deleteUser(userId: number) {
//   if (sessionStorage.user !== null) {
//       var tmpUser = JSON.parse(sessionStorage.user);
//       sessionStorage.removeItem('user');
//       return await fetch(`/api/users/` + userId, {
//           method: 'DELETE',
//           headers: {
//               'Authorization': `Bearer ${tmpUser.token}`
//           }
//       }).then(checkStatus).catch((error: TypeError) => {
//           throw new Error(error.message);
//       });
//   } else {
//       throw new Error("Not logged on")
//   }
// }

export async function putUser(password: string, pseudo: string) {
  if (sessionStorage.user !== null) {
      var tmpUser = JSON.parse(sessionStorage.user);
      return await fetch(`/api/users/` + tmpUser.id, {
          method: 'PUT',
          body: JSON.stringify({id:tmpUser.id ,email: tmpUser.email, password, pseudo }),
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${tmpUser.token}`
          }
      }).then(checkStatus).catch((error: TypeError) => {
          throw new Error(error.message);
      });
  } else {
      throw new Error("Not logged on")
  }
}