import { Story } from "../models/story";
import { User } from "../models/user";

async function checkStatus(response: any) {
    if (response.ok) {
        return response;
    } else {
        if (response.status === 400) {
            await response.json().then((jsonError: any) => {
                console.log(jsonError.detail);
                if (jsonError.detail.title !== undefined)
                    throw new Error(jsonError.detail.title);
                throw new Error("An error occurred");
            });
        }
        throw new Error("An error occurred");
    }
}

function convertToStoryModels(data: any[]): Story[] {
    let stories: Story[] = data.map(convertToStoryModel);
    return stories;
}

function convertToStoryModel(item: any): Story {
    return new Story(item);
}

function parseJSON(response: Response) {
    return response.json();
}

export async function getAllStories() {
    return await fetch('/api/stories')
        .then(checkStatus).then(parseJSON)
        .then(convertToStoryModels)
        .catch((error: TypeError) => {
            //console.log("error detected")
            throw new Error(error.message);
        });;
}

export async function getStory(storyId: number) {
    return await fetch('/api/stories/' + storyId)
        .then(checkStatus).then(parseJSON)
        .then(convertToStoryModel)
        .catch((error: TypeError) => {
            throw new Error(error.message);
        });
}

export async function getStoryStats(storyId: number) {
    return await fetch('/api/stories/stats/'+storyId)
        .then(checkStatus).then(parseJSON)
        .catch((error: TypeError) => {
            throw new Error(error.message);
        });
}

export async function getAllStoriesForUser(id: number) {
    if (sessionStorage.user !== null) {
        var tmpUser = JSON.parse(sessionStorage.user);
        return await fetch('/api/stories/from/' + id, {
            headers: {
                'Authorization': `Bearer ${tmpUser.token}`
            }
        })
            .then(checkStatus).then(parseJSON)
            .then(convertToStoryModels)
            .catch((error: TypeError) => {
                throw new Error(error.message);
            });;
    } else {
        throw new Error("Not logged on")
    }
}

export async function deleteStory(storyId: number, userId: number) {
    if (sessionStorage.user !== null) {
        var tmpUser = JSON.parse(sessionStorage.user);
        return await fetch(`/api/stories/` + storyId + '/' + userId, {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${tmpUser.token}`
            }
        }).then(checkStatus).catch((error: TypeError) => {
            throw new Error(error.message);
        });
    } else {
        throw new Error("Not logged on")
    }
}

export async function postStory(user: User, summary: string, title: string) {
    if (sessionStorage.user !== null) {
        var tmpUser = JSON.parse(sessionStorage.user);
        return await fetch(`/api/stories`, {
            method: 'POST',
            body: JSON.stringify({ 'Summary': summary, 'Author': user, 'Title': title }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tmpUser.token}`
            }
        }).then(checkStatus).catch((error: TypeError) => {
            throw new Error(error.message);
        });
    } else {
        throw new Error("Not logged on")
    }
}

export async function putStory(story: any) {
    if (sessionStorage.user !== null) {
        var tmpUser = JSON.parse(sessionStorage.user);
        return await fetch(`/api/stories/` + story.storyId, {
            method: 'PUT',
            body: JSON.stringify(story),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tmpUser.token}`
            }
        }).then(checkStatus).catch((error: TypeError) => {
            throw new Error(error.message);
        });
    } else {
        throw new Error("Not logged on")
    }
}

export async function movePlayers(from: number, to: number) {
    if (sessionStorage.user !== null) {
        var tmpUser = JSON.parse(sessionStorage.user);
        return await fetch(`/api/players/movePlayers/` + from + '/' + to, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${tmpUser.token}`,
                'Content-Type': 'application/json'
            }
        }).then(checkStatus).catch((error: TypeError) => {
            throw new Error(error.message);
        });
    } else {
        throw new Error("Not logged on")
    }
}