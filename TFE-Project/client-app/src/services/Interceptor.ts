export const interceptor = async () => {
    const { fetch: originalFetch } = window;
    console.log("intercepted");
    window.fetch = async (...args) => {
        let [resource, config] = args;
        //console.log(args);
        // if (sessionStorage.user !== null && sessionStorage.user !== undefined) {
        //     var tmpUser = JSON.parse(sessionStorage.user);
        //     if (tmpUser.token) {
        //         config.headers = {
        //             'Authorization': `Bearer ${tmpUser.token}`
        //         }
        //     }
        // }
        //console.log(resource, config);
        const response = await originalFetch(resource, config);
        return response;
    };
};