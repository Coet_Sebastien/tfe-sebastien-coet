// import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";
import Home from "./pages/Home";
import LoginPage from './pages/LoginPage';
import UserSettings from './pages/UserSettings';
import { MyStories } from './pages/MyStories';
import { StoryBuilder } from './pages/StoryBuilder';
import { interceptor } from './services/Interceptor';
import { useEffect } from 'react';

function App() {

  useEffect(() => {
    interceptor();
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="login" element={<LoginPage />} />
          <Route path="userSettings" element={<UserSettings />} />
          <Route path="mystories/" element={<MyStories />} />
          <Route path="storybuilder/:id" element={<StoryBuilder />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
