import { useRef, useEffect } from "react";

function usePrevious(value: any) {
    const ref = useRef<any>();
    const prev = useRef<any>();
    useEffect(() => {
        var valueString = JSON.stringify(value);
        if (valueString!==ref.current) {
            prev.current = ref.current;
            ref.current = valueString;
        }
    }, [value]);
    return prev.current;
}
export default usePrevious;