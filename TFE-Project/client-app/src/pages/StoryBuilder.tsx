import { useParams } from "react-router-dom";
import { useState, useEffect, useCallback, useRef, SyntheticEvent } from "react";
import { Story } from "../models/story";
import { User } from "../models/user";
import { BarChartLine, Person } from "react-bootstrap-icons";
import { getStory } from "../services/StoryService";
import { useNavigate } from "react-router-dom";
import { Modal, Table, Form, Toast, Button } from "react-bootstrap";
import { useContext } from "react";
import { UserContext } from "./Layout";
import ReactFlow, {
    Node,
    Edge,
    FitViewOptions,
    useEdgesState,
    useNodesState,
    updateEdge,
    Connection,
    OnConnect,
    MiniMap,
    Controls,
    Panel,
    isNode,
    isEdge,
    getIncomers,
    getOutgoers,
    getConnectedEdges,
    addEdge,
    OnConnectStart,
    OnConnectStartParams,
    OnConnectEnd,
    OnInit,
    ReactFlowProvider,
    ReactFlowInstance,
    MarkerType,
    ControlButton,
    EdgeTypes
} from 'reactflow';
import { putStory, movePlayers, getStoryStats } from "../services/StoryService";
import { DepartNode } from "../components/DepartNode";
import { ScenarioNode } from "../components/ScenarioNode";
import { CombatNode } from "../components/CombatNode";
import { EnduranceChangeEventNode } from "../components/EnduranceChangeEventNode";
import usePrevious from "../util/usePrevious";

const nodeTypes = {
    departNode: DepartNode,
    scenarioNode: ScenarioNode,
    combatNode: CombatNode,
    enduranceChangeEventNode: EnduranceChangeEventNode
};

export function StoryBuilder() {

    const params = useParams();
    const storyId = Number(params.id);
    const {
        currentUser,
        setCurrentUser
    } = useContext(UserContext);
    const [story, setStory] = useState<Story | undefined>(undefined);
    const [summary, setSummary] = useState("");
    const [title, setTitle] = useState("");
    const [validated, setValidated] = useState(false);
    const [showToast, setShowToast] = useState(false);
    const [preventSave, setPreventSave] = useState(false);

    // ------------------- PLAYERS MANAGEMENT PART --------------------------------

    const [showPlayersPanel, setShowPlayersPanel] = useState<boolean>(false);
    const [playersMigrations, setPlayersMigrations] = useState<{ from: number, to: number }[]>([]);
    let [fromForm, setFromForm] = useState(-1);
    let [toForm, setToForm] = useState(-1);

    const showMovePlayersPanel = () => {
        setShowPlayersPanel(true);
    };

    const hideMovePlayersPanel = () => {
        setShowPlayersPanel(false);
    }

    const handleSubmitMigration = (event: SyntheticEvent) => {
        event.preventDefault();
        if (toForm > -1 && fromForm > -1)
            setPlayersMigrations((pms) => {
                return [...pms, { from: fromForm, to: toForm }];
            });
        //console.log(toForm, fromForm, playersMigrations);
    }

    const removeMigration = (from: number, to: number) => {
        setPlayersMigrations((pms) => {
            return pms.filter(pm => pm.from !== from && pm.to !== to);
        });
    }

    // ------------------- REACT FLOW PART --------------------------------

    const [nodes, setNodes, onNodesChange] = useNodesState<Node[]>([]);
    const [edges, setEdges, onEdgesChange] = useEdgesState<Edge[]>([]);

    //to do: add custom nodes and edges for a better control

    const fitViewOptions: FitViewOptions = {
        padding: 0.2,
    };

    const minimapStyle = {
        height: 120,
    };

    // -------- CONTROLS --------

    const [selected, setSelected] = useState<Node | Edge>();
    const [elementText, setElementText] = useState("");
    const [elementAbility, setElementAbility] = useState(0);
    const [elementEndurance, setElementEndurance] = useState(0);
    const [typeToAdd, setTypeToAdd] = useState(0);
    const [helpPanel, setHelpPanel] = useState<boolean>(false);
    const [showStats, setShowStats] = useState<boolean>(false);
    const [isConnectable, setIsConnectable] = useState(true);
    const [isSelectable, setIsSelectable] = useState(true);
    const dataStats = useRef<any>();

    async function loadStats() {
        console.log("Requesting Stats");
        await getStoryStats(storyId).then((data) => {
            //loading the data for stats inside the ref to avoid issues if re-rendering
            dataStats.current=data;
            setEdges((eds) => {
                var a = eds.map((edge) => {
                    dataStats.current.statRelsDTO.forEach((statRel: any) => {
                        if (statRel.input.toString() === edge.source && statRel.output.toString() === edge.target)
                            edge.label = "Total: " + statRel.totalPlayers;
                    });
                    return edge;
                })
                return a;
            });
            setNodes((nds) => {
                var a = nds.map((node) => {
                    dataStats.current.statChaptersDTO.forEach((statChap: any) => {
                        if (node.id === statChap.chapterId.toString()) {
                            switch (node.type) {
                                case "combatNode":
                                case "enduranceChangeEventNode":
                                    var tmpData = {
                                        ...node.data,
                                        stats: {
                                            deaths: statChap.deaths,
                                            totalPlayers: statChap.totalPlayers,
                                            id: statChap.chapterId
                                        },
                                        nbCurrentPlayers: statChap.nbCurrentPlayers
                                    };
                                    node.data = tmpData;
                                    break;
                                default:
                                    var tmpData2 = {
                                        ...node.data,
                                        stats: {
                                            quits: statChap.quits,
                                            totalPlayers: statChap.totalPlayers,
                                            id: statChap.chapterId
                                        },
                                        nbCurrentPlayers: statChap.nbCurrentPlayers
                                    };
                                    node.data = tmpData2;
                                    break;
                            }
                        }
                    });
                    return node;
                });
                return a;
            });
        });
    }


    const changeShowStats = () => {
        if (showStats) {
            setShowStats(false);
            setIsConnectable(true);
            setIsSelectable(true);
            if (prevEdges !== undefined)
                setEdges(JSON.parse(prevEdges));
            setNodes((nds) => {
                return nds.map((node) => {
                    var tmpData = {
                        ...node.data,
                        stats: undefined
                    };
                    node.data = tmpData;
                    return node;
                })
            });
            setPreventSave(false);
        } else {
            setPreventSave(true);
            setIsConnectable(false);
            setIsSelectable(false);
            setShowStats(true);
            loadStats()
        }

    }

    const prevEdges = usePrevious(edges);



    const hideHelpPanel = () => {
        setHelpPanel(false);
    }

    const showHelpPanel = () => {
        setHelpPanel(true);
    }

    //to modify by useOnSelectionChange

    useEffect(() => {
        if (!showStats) {
            edges.forEach(edge => {
                if (edge.selected) {
                    setSelected(edge);
                }
            });
            nodes.forEach(node => {
                if (node.selected) {
                    //console.log(selected)
                    setSelected(node);
                }
            });
        }
    }, [nodes, edges])

    useEffect(() => {
        if (!showStats) {
            //console.log("selected: " + selected?.id);
            //console.log("deletable: " + selected?.deletable);
            if (selected !== undefined && isNode(selected) && (selected.type === "scenarioNode" || selected.type === "departNode"))
                setElementText(selected.data.label);
            if (selected !== undefined && isNode(selected) && selected.type === "combatNode") {
                setElementAbility(selected.data.ability);
                setElementEndurance(selected.data.endurance);
            }
            if (selected !== undefined && isNode(selected) && selected.type === "enduranceChangeEventNode") {
                setElementEndurance(selected.data.enduranceModifier);
            }
            if (selected !== undefined && isEdge(selected) && typeof selected.label == "string")
                setElementText(selected.label);
        }
    }, [selected])

    useEffect(() => {
        if (!showStats) {
            if (selected !== undefined && isNode(selected)) {
                setNodes((nds) =>
                    nds.map((node) => {
                        if (node.id === selected.id) {
                            var tmpData = {
                                ...node.data,
                                label: elementText,
                            };
                            node.data = tmpData;
                        }

                        return node;
                    })
                );
            }
            if (selected !== undefined && isEdge(selected)) {
                setEdges((eds) =>
                    eds.map((edge) => {
                        if (edge.id === selected.id) {
                            edge.label = elementText
                        }
                        return edge;
                    })
                );
            }
        }
    }, [elementText, setNodes]);


    //combat
    useEffect(() => {
        if (!showStats) {
            if (selected !== undefined && isNode(selected)) {
                setNodes((nds) =>
                    nds.map((node) => {
                        if (node.id === selected.id && selected.type === "combatNode") {
                            var tmpData = {
                                ...node.data,
                                endurance: elementEndurance,
                                ability: elementAbility
                            };
                            node.data = tmpData;
                        }
                        if (node.id === selected.id && selected.type === "enduranceChangeEventNode") {
                            var tmpData2 = {
                                ...node.data,
                                enduranceModifier: elementEndurance
                            };
                            node.data = tmpData2;
                        }
                        return node;
                    })
                );
            }
        }
    }, [elementAbility, elementEndurance, setNodes]);

    const nodeColor = (node: Node) => {
        if (node.selected)
            return 'purple'
        switch (node.type) {
            case 'scenarioNode':
                return 'lightgreen';
            case 'departNode':
                return 'lightgreen';
            case 'combatNode':
                if ((getOutgoers(node, nodes, edges)).length !== 1) {
                    return 'red';
                }
                return 'lightblue';
            case 'enduranceChangeEventNode':
                if ((getOutgoers(node, nodes, edges)).length !== 1) {
                    return 'red';
                }
                return 'lightsalmon';
            default:
                return 'lightgreen'
        }
    };

    // -------- EDGES --------

    const onEdgeUpdate = useCallback(
        (oldEdge: Edge, newConnection: Connection) => {
            setEdges((eds) => updateEdge(oldEdge, newConnection, eds));
            onEdgeChangeCheckDeletable();
        }, [setEdges, edges]
    );

    const onConnect: OnConnect = useCallback(
        (connection) => {
            setEdges((eds) => addEdge(connection, eds))
            onEdgeChangeCheckDeletable();
            //console.log("Connected OK ");
        }, [setEdges, edges]
    );

    const onEdgesDelete = useCallback(
        (deleted: Edge[]) => {
            setSelected(undefined);
            var newEdges = [...edges];
            setEdges(newEdges.filter(e => deleted.find(del => e.id === del.id) === undefined));
            onEdgeChangeCheckDeletable();
        }, [setEdges, edges]
    );

    //On change (add, remove, update), it needs to check if edges connected to the same target need to be updated for deletable.
    //Also check if all edges have a label and id formatting is correct
    const onEdgeChangeCheckDeletable = useCallback(
        () => {
            if (!showStats) {
                setEdges((eds) => {
                    onEdgeChangeCheckNodes(eds);
                    return eds.map((edge) => {
                        edge.deletable = eds.filter(e => e.target === edge.target).length > 1;
                        if (edge.label === undefined) {
                            edge.label = "New option";
                        };
                        edge.id = edge.source + "-" + edge.target;
                        edge.style = {
                            strokeWidth: 2,
                            stroke: 'black'
                        };
                        edge.markerEnd = {
                            type: MarkerType.ArrowClosed,
                            color: 'black',
                        };
                        if (edge.type === undefined) {
                            edge.type = "straight";
                        };
                        if (edge.className === undefined) {
                            edge.className = "text-wrap";
                        }
                        return edge;
                    })
                }
                );
            }
        }, [setEdges, edges]
    );

    const onEdgeChangeCheckNodes = useCallback((eds: Edge[]) => {
        if (!showStats) {
            //console.log("proc");
            setPreventSave(false);
            setNodes((nds) =>
                nds.map((node) => {
                    if (node.type === "combatNode" || node.type === "enduranceChangeEventNode") {
                        var linkedEdges = eds.filter(e => e.source === node.id);
                        if (linkedEdges.length === 1) {
                            //console.log("node n°" + node.id + " OK");
                            var nodeVictory = nds.filter(n => n.id === linkedEdges[0].target);
                            if (nodeVictory[0].type === "scenarioNode") {
                                var tmpData = {
                                    ...node.data,
                                    showAlert: false
                                };
                                node.data = tmpData;
                            } else {
                                var tmpData = {
                                    ...node.data,
                                    showAlert: true
                                };
                                node.data = tmpData;
                                setPreventSave(true);
                            }

                        } else {
                            //console.log("node n°" + node.id + " need a victory");
                            //console.log(eds.filter(e => e.source === node.id).length);
                            //console.log(eds);
                            var tmpData = {
                                ...node.data,
                                showAlert: true
                            };
                            node.data = tmpData;
                            setPreventSave(true);
                        }
                    }
                    return node;
                })
            );
        }
    }, [setNodes, nodes, setEdges, edges])

    // -------- NODES --------

    const onNodesDelete = useCallback(
        (deleted: Node[]) => {
            setSelected(undefined);
            setEdges(
                deleted.reduce((acc, node) => {
                    const incomers = getIncomers(node, nodes, edges);
                    const outgoers = getOutgoers(node, nodes, edges);
                    const connectedEdges = getConnectedEdges([node], edges);

                    const remainingEdges = acc.filter((edge) => !connectedEdges.includes(edge));

                    const createdEdges = incomers.flatMap(({ id: source }) =>
                        outgoers.map(({ id: target }) => ({ id: `${source}->${target}`, source, target, label: "New option" }))
                    );

                    return [...remainingEdges, ...createdEdges];
                }, edges)
            );
            onEdgeChangeCheckDeletable();
        },
        [nodes, edges]
    );

    //Create a node on edge drop

    const [id, setId] = useState(-1);
    const connectingNodeId = useRef<string | null>(null);
    const [reactFlowInstance, setReactFlowInstance] = useState<ReactFlowInstance>();

    const onConnectStart: OnConnectStart = useCallback(
        (event: SyntheticEvent, props: OnConnectStartParams) => {
            var { nodeId, handleId, handleType } = props;
            connectingNodeId.current = nodeId;
            //console.log("Connecting start ");
        }, []);

    const onInit: OnInit = useCallback(
        (reactFlowInstanceToSet: ReactFlowInstance) => {
            setReactFlowInstance(reactFlowInstanceToSet);
        },
        [])

    const onConnectEnd: OnConnectEnd =
        (event: any) => {
            const targetIsPane = event.target.classList.contains('react-flow__pane');
            if (reactFlowInstance && connectingNodeId.current !== null && targetIsPane) {
                //console.log("Connecting end ");
                var viewportPosition = { y: event.target.getBoundingClientRect().top, x: event.target.getBoundingClientRect().left };
                var origin = reactFlowInstance.getNode(connectingNodeId.current);
                var position = reactFlowInstance.project({
                    x: (event.clientX - viewportPosition.x),
                    y: (event.clientY - viewportPosition.y) - 20,
                });
                var newId = id;
                setId(id - 1);
                var newNode: Node = {
                    id: newId.toString(),
                    position: position,
                    data: {},
                    className: "text-wrap",
                    style: { maxWidth: "200px" }
                };
                switch (typeToAdd) {
                    case 0:
                        newNode.type = "scenarioNode"
                        newNode.data = { label: "New node", nbCurrentPlayers: 0 }
                        //console.log('Adding scenario node ' + typeToAdd);
                        break;
                    case 1:
                        newNode.type = "combatNode"
                        newNode.data = { endurance: 25, ability: 15, nbCurrentPlayers: 0, showAlert: true }
                        //console.log('Adding combat node ' + typeToAdd)
                        break;
                    case 2:
                        newNode.type = "enduranceChangeEventNode"
                        newNode.data = { enduranceModifier: 0, nbCurrentPlayers: 0, showAlert: true }
                        //console.log('Adding combat node ' + typeToAdd)
                        break;
                }
                setNodes((nds) => {
                    //console.log(typeToAdd);
                    return [...nds, newNode];
                });
                if (connectingNodeId.current !== null) {
                    var edgeId: string = connectingNodeId.current + "-" + newNode.id;
                    var newEdge: Edge = {
                        id: edgeId,
                        source: connectingNodeId.current,
                        target: newNode.id,
                        deletable: false,
                        label: "New option"
                    };
                    setEdges((eds) => [...eds, newEdge]);
                }
                onEdgeChangeCheckDeletable();

            }
        }

    // ---------------------- LOADING PART ---------------------------

    const navigate = useNavigate();
    const navigateHome = () => {
        // 👇️ navigate to /
        navigate('/');
    };

    useEffect(() => {
        if (currentUser === undefined && sessionStorage.user === undefined) {
            navigateHome();
        } else {
            loadStory();
        }
    }, [storyId, currentUser])

    useEffect(() => {
        if (currentUser === undefined && sessionStorage.user === undefined) {
            navigateHome();
        } else {
            loadStory();
        }
    }, [])

    // useEffect(() => {
    //     if (story !== undefined)
    //         convertToNodesAndEdges(story);
    // }, [story])

    const [error, setError] = useState<string | undefined>(undefined);

    async function loadStory() {
        if (currentUser !== undefined && currentUser.id) {
            console.log("Fetching story ...")
            await getStory(storyId).then((data) => {
                //check if user==author
                if (data.author === undefined || data.author.id !== currentUser.id) {
                    navigateHome();
                } else {
                    setEdges([]);
                    setNodes([]);
                    setStory(data);
                    convertToNodesAndEdges(data);
                    if(reactFlowInstance)
                        reactFlowInstance.fitView();
                    setTitle(data.title);
                    setSummary(data.summary);
                    setValidated(false);
                    setSelected(undefined);
                    setElementText("");
                    setElementAbility(0);
                    setElementEndurance(0);
                    setFromForm(-1);
                    setToForm(-1);
                    setTypeToAdd(0);
                    setPlayersMigrations([]);
                    setShowStats(false);
                    console.log(data);
                }

            });
        }
    }

    async function convertToNodesAndEdges(storyToConvert: Story) {
        if (storyToConvert.chapters.length > 0) {
            console.log("Starting convertion ...")
            var tmpNodes: Node[] = [];
            var tmpEdges: Edge[] = [];
            storyToConvert.chapters.forEach((chapterToConvert) => {
                if (storyToConvert.chapters[0].id === chapterToConvert.id) {
                    tmpNodes.push({
                        id: JSON.stringify(chapterToConvert.id),
                        data: {
                            label: chapterToConvert.body,
                            nbCurrentPlayers: chapterToConvert.nbCurrentPlayers
                        },
                        position: {
                            y: chapterToConvert.y,
                            x: chapterToConvert.x
                        },
                        //sourcePosition: Position["Right"],
                        type: 'departNode',
                        deletable: false,
                        className: "text-wrap",
                        style: { maxWidth: "200px" }
                    });
                }
                else {
                    var newNode: Node = {
                        id: JSON.stringify(chapterToConvert.id),
                        data: {},
                        position: {
                            y: chapterToConvert.y,
                            x: chapterToConvert.x
                        },
                        //sourcePosition: Position["Right"],
                        className: "text-wrap",
                        style: { maxWidth: "200px" }
                    };
                    switch (chapterToConvert.type) {
                        case "Scenario":
                            newNode.type = "scenarioNode"
                            newNode.data = { label: chapterToConvert.body, nbCurrentPlayers: chapterToConvert.nbCurrentPlayers }
                            //console.log('Adding scenario node ' + typeToAdd);
                            break;
                        case "Combat":
                            newNode.type = "combatNode"
                            newNode.data = { showAlert: false, endurance: chapterToConvert.endurance, ability: chapterToConvert.ability, nbCurrentPlayers: chapterToConvert.nbCurrentPlayers }
                            //console.log('Adding combat node ' + typeToAdd)
                            break;
                        case "EnduranceChangeEvent":
                            newNode.type = "enduranceChangeEventNode"
                            newNode.data = { showAlert: false, enduranceModifier: chapterToConvert.enduranceModifier, nbCurrentPlayers: chapterToConvert.nbCurrentPlayers }
                            //console.log('Adding combat node ' + typeToAdd)
                            break;
                    }
                    tmpNodes.push(newNode);
                }
            })
            //console.log(tmpNodes);
            setNodes(tmpNodes);
            storyToConvert.relationships.forEach((r) => {
                var output = storyToConvert.chapters.find(c => c.id === r.output);
                if (output !== undefined) {
                    tmpEdges.push({
                        id: r.input + "-" + r.output,
                        source: JSON.stringify(r.input),
                        target: JSON.stringify(r.output),
                        label: r.body,
                        updatable: 'source',
                        deletable: output.incomingRelationships.length > 1,
                        style: {
                            strokeWidth: 2,
                            stroke: 'black'
                        },
                        markerEnd: {
                            type: MarkerType.ArrowClosed,
                            color: 'black',
                        },
                        type: "straight"
                    })
                }
            })
            //console.log(tmpEdges);
            setEdges(tmpEdges);
        }
    }

    /*
    var positionsX: { distance: number[], chapterId: number }[];
    var positionsY: { y: number, chapterId: number }[];

    function calculatePositions(storyToConvert: Story): { x: number, y: number, chapterId: number }[] {
        return storyToConvert.chapters.map((chapter) => {
            return {
                x: calculateXfor(relationshipsTabToChaptersTabForInputs(chapter.incomingRelationships)),
                y: calculateYfor(relationshipsTabToChaptersTabForInputs(chapter.incomingRelationships), chapter.id),
                chapterId: chapter.id
            };
        })
        // positionsX = [];
        // positionsY = [];
        // var beginningChapter = storyToConvert.chapters.find(c => c.id === storyToConvert.beginningChapterId);
        // if (beginningChapter !== undefined)
        //     distancesFrom(relationshipsTabToChaptersTabForOutputs(beginningChapter.outgoingRelationships), 0);
        // //console.log(positionsX);
        // var deltaInOut: { delta: number, chapterId: number }[] = storyToConvert.chapters.map((chapter) => {
        //     return {
        //         delta: chapter.incomingRelationships.length - chapter.outgoingRelationships.length,
        //         chapterId: chapter.id
        //     };
        // });
        // //console.log(deltaInOut);
        // storyToConvert.chapters.forEach(chapter => {
        //     deltaWithParentsPosition(relationshipsTabToChaptersTabForInputs(chapter.incomingRelationships), deltaInOut, chapter.id);
        // });
        // //console.log(positionsY);
        // return storyToConvert.chapters.map((chapter) => {
        //     var chapterIndexInArrayX = positionsX.findIndex(p => p.chapterId === chapter.id);
        //     var chapterIndexInArrayY = positionsY.findIndex(p => p.chapterId === chapter.id);
        //     if (chapterIndexInArrayX !== -1 && chapterIndexInArrayY !== -1) {
        //         var x = 0;
        //         positionsX[chapterIndexInArrayX].distance.forEach(d => {
        //             x += d;
        //         });
        //         return {
        //             x: x / positionsX[chapterIndexInArrayX].distance.length,
        //             y: 0-positionsY[chapterIndexInArrayY].y,
        //             chapterId: chapter.id
        //         }
        //     }
        //     return { x: 0, y: 0, chapterId: chapter.id };
        // });
    }

    function distancesFrom(chapters: Chapter[], distancetoZero: number) {
        if (chapters.length > 0) {
            distancetoZero++;
            chapters.forEach(chapter => {
                var chapterIndexInArray = positionsX.findIndex(p => p.chapterId === chapter.id);
                if (chapterIndexInArray === -1) {
                    positionsX.push({
                        distance: [distancetoZero],
                        chapterId: chapter.id
                    });
                } else {
                    positionsX[chapterIndexInArray].distance.push(distancetoZero);
                }
                distancesFrom(relationshipsTabToChaptersTabForOutputs(chapter.outgoingRelationships), distancetoZero);
            });
        }
    }

    function deltaWithParentsPosition(chapters: Chapter[], deltaInOut: { delta: number, chapterId: number }[], chapterId: number): number {
        if (chapters.length > 0) {
            var chapterInDelta = deltaInOut.find(c => c.chapterId === chapterId);
            if (chapterInDelta !== undefined) {
                var sum = 0;
                chapters.forEach(chapter => {
                    var chapterIndexInArray = positionsY.findIndex(p => p.chapterId === chapter.id);
                    //if Y is not already calculated
                    if (chapterIndexInArray === -1) {
                        sum += deltaWithParentsPosition(relationshipsTabToChaptersTabForInputs(chapter.incomingRelationships), deltaInOut, chapter.id);
                    }
                    //if Y is calculated for the chapter
                    if (chapterIndexInArray !== -1) {
                        sum += positionsY[chapterIndexInArray].y;
                    }
                });
                var tot = sum / chapters.length + chapterInDelta.delta;
                var chapterIndexInArrayY = positionsY.findIndex(p => p.chapterId === chapterId);
                if (chapterIndexInArrayY === -1)
                    positionsY.push({ y: tot, chapterId: chapterId });
                return sum;
            }
        }
        return 0;
    }

    //V1
    function calculateXfor(incomingChapters: Chapter[]): number {
        if (incomingChapters.length === 0) {
            return 0;
        }
        else {
            var sum = incomingChapters.length;
            incomingChapters.forEach((c) => {
                var toSend = relationshipsTabToChaptersTabForInputs(c.incomingRelationships);
                if (toSend !== undefined) {
                    sum += calculateXfor(toSend);
                }
            })
            var tot = sum / incomingChapters.length;
            //console.log("X:" + tot);
            return tot;
        }
    }

    function calculateYfor(parents: Chapter[], chapterId: number): number {
        if (parents.length === 0) {
            //reached the beginning
            return 0;
        }
        else {
            var tot = 0;
            parents.forEach(parent => {
                var totalOfDescendants = countDescendantsOf(relationshipsTabToChaptersTabForOutputs(parent.outgoingRelationships));
                var indexOfChapterInParent = parent.outgoingRelationships.findIndex((r) => r.output === chapterId);
                var nbOfDescendants = parent.outgoingRelationships.length;
                var positionYofParent = calculateYfor(relationshipsTabToChaptersTabForInputs(parent.incomingRelationships), parent.id);
                //if index 0, will be at the same level as parent
                tot += positionYofParent + totalOfDescendants / nbOfDescendants * indexOfChapterInParent
            });
            var nbOfParents = parents.length;
            return tot / nbOfParents;
        }
    }

    function countDescendantsOf(descendants: Chapter[]): number {
        if (descendants.length === 0) return 0;
        var sum = 0;
        descendants.forEach(descendant => {
            sum += descendant.outgoingRelationships.length + countDescendantsOf(relationshipsTabToChaptersTabForOutputs(descendant.outgoingRelationships));
        });
        //v1
        var tot = sum / descendants.length;
        //v2 easier
        //var tot = sum;
        return tot;
    }

    function relationshipsTabToChaptersTabForInputs(relationships: Relationship[]): Chapter[] {
        var tmpTab: Chapter[] = [];
        if (relationships.length > 0) {
            relationships.forEach((r) => {
                var chapterTmp = story?.chapters.find(x => x.id === r.input)
                if (chapterTmp !== undefined) {
                    tmpTab.push(chapterTmp);
                }
            })
        }
        //console.log(tmpTab);
        return tmpTab;
    }

    function relationshipsTabToChaptersTabForOutputs(relationships: Relationship[]): Chapter[] {
        var tmpTab: Chapter[] = [];
        if (relationships.length > 0) {
            relationships.forEach((r) => {
                var chapterTmp = story?.chapters.find(x => x.id === r.output)
                if (chapterTmp !== undefined) {
                    tmpTab.push(chapterTmp);
                }
            })
        }
        //console.log(tmpTab);
        return tmpTab;
    }*/

    // ---------------------- SAVE/CANCEL PART ---------------------------

    const sendStoryToDB = (event: any) => {
        event.preventDefault();
        console.log("Saving");
        const form = event.currentTarget;
        setError('');
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
            setValidated(true);
        } else {
            //first, deal with players migrations
            if (playersMigrations.length > 0) {
                playersMigrations.forEach(pm => {
                    movePlayers(pm.from, pm.to).catch((er: TypeError) => {
                        setError(er.message);
                    });
                });
            }
            //then put story
            var storyToSend = {
                storyId: story?.storyId,
                author: currentUser,
                title: title,
                summary: summary,
                beginningChapterId: story?.beginningChapterId,
                nbCurrentPlayers: 0,
                chapters: nodes.map((node: Node) => {
                    switch (node.type) {
                        case "combatNode":
                            if ((getOutgoers(node, nodes, edges)).length === 1) {
                                return {
                                    id: node.id,
                                    storyId: story?.storyId,
                                    x: Math.round(Number(node.position.x)),
                                    y: Math.round(Number(node.position.y)),
                                    ability: Number(node.data.ability),
                                    endurance: Number(node.data.endurance),
                                    victoryId: Number((getOutgoers(node, nodes, edges)).at(0)?.id),
                                    //false data
                                    incomingRelationships: [],
                                    outgoingRelationships: [],
                                    nbCurrentPlayers: 0,
                                    type: "Combat"
                                };
                            }
                            return {};
                        case "enduranceChangeEventNode":
                            if ((getOutgoers(node, nodes, edges)).length === 1) {
                                return {
                                    id: node.id,
                                    storyId: story?.storyId,
                                    x: Math.round(Number(node.position.x)),
                                    y: Math.round(Number(node.position.y)),
                                    enduranceModifier: Number(node.data.enduranceModifier),
                                    victoryId: Number((getOutgoers(node, nodes, edges)).at(0)?.id),
                                    //false data
                                    incomingRelationships: [],
                                    outgoingRelationships: [],
                                    nbCurrentPlayers: 0,
                                    type: "EnduranceChangeEvent"
                                };
                            }
                            return {};
                        case "scenarioNode":
                            return {
                                id: node.id,
                                body: node.data.label,
                                storyId: story?.storyId,
                                x: Math.round(Number(node.position.x)),
                                y: Math.round(Number(node.position.y)),
                                //false data
                                incomingRelationships: [],
                                outgoingRelationships: [],
                                nbCurrentPlayers: 0,
                                type: "Scenario"
                            };
                        case "departNode":
                            return {
                                id: node.id,
                                body: node.data.label,
                                storyId: story?.storyId,
                                x: Math.round(Number(node.position.x)),
                                y: Math.round(Number(node.position.y)),
                                //false data
                                incomingRelationships: [],
                                outgoingRelationships: [],
                                nbCurrentPlayers: 0,
                                type: "Scenario"
                            };
                    }
                    return {
                        id: node.id,
                        body: node.data.label,
                        storyId: story?.storyId,
                        x: Number(node.position.x),
                        y: Number(node.position.y),
                        //false data
                        incomingRelationships: [],
                        outgoingRelationships: [],
                        nbCurrentPlayers: 0
                    };
                }),
                relationships: edges.map((edge: Edge) => {
                    return {
                        input: edge.source,
                        output: edge.target,
                        body: edge.label,
                        storyId: story?.storyId
                    };
                }),
            };
            console.log(storyToSend);
            putStory(storyToSend).then(loadStory).then(() => setValidated(true)).catch((er: TypeError) => {
                console.log("ERROR:" + er.message)
                setError(er.message);
            });
            setValidated(true);
            setShowToast(true);
        }
    };

    let testButton = () => {
        console.log(nodes);
        console.log(edges);
        console.log(id);
        console.log(error);
    }

    // ---------------------- RENDER PART ---------------------------

    return (
        <>
            <main className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen", minHeight: "90vh" }}>
                {error
                    ? (
                        <Toast onClose={() => setShowToast(false)} show={showToast} className="position-fixed top-50 start-50 translate-middle-x bg-danger"
                            style={{ width: "200px", zIndex: 999 }}>
                            <Toast.Header></Toast.Header>
                            <Toast.Body className="text-center">
                                {error}
                            </Toast.Body>
                        </Toast>
                    )
                    : (
                        <Toast onClose={() => setShowToast(false)} show={showToast} delay={1500} autohide className="position-fixed top-50 start-50 translate-middle-x"
                            style={{ width: "200px", zIndex: 999 }}>
                            <Toast.Body className="text-center bg-success text-light">
                                Saving
                            </Toast.Body>
                        </Toast>
                    )
                }
                {/* <button onClick={testButton}>test</button> */}
                <div className='m-4 p-4'>
                    {nodes.length > 0 &&
                        <>
                            <ReactFlowProvider>
                                <ReactFlow
                                    className=' border w-auto h-auto'
                                    style={{ minHeight: "600px", backgroundColor: "white" }}
                                    nodes={nodes}
                                    edges={edges}
                                    onNodesChange={onNodesChange}
                                    onEdgesChange={onEdgesChange}
                                    elementsSelectable={isSelectable}
                                    nodesConnectable={isConnectable}
                                    fitView={true}
                                    minZoom={0.10}
                                    onEdgeUpdate={onEdgeUpdate}
                                    onNodesDelete={onNodesDelete}
                                    onEdgesDelete={onEdgesDelete}
                                    onConnect={onConnect}
                                    onConnectStart={onConnectStart}
                                    onConnectEnd={onConnectEnd}
                                    onInit={onInit}
                                    nodeTypes={nodeTypes}
                                >
                                    <MiniMap style={minimapStyle} nodeColor={nodeColor} zoomable pannable />
                                    <Controls showInteractive={false}>
                                        <ControlButton onClick={changeShowStats} title={showStats ? "Hide stats" : "Show stats"}>
                                            <BarChartLine />
                                        </ControlButton>
                                        <ControlButton onClick={showHelpPanel} title="help">
                                            <div><strong>?</strong></div>
                                        </ControlButton>
                                    </Controls>
                                    <Panel position="top-right" style={{ backgroundColor: "darkgrey" }}>
                                        {selected?.type === "combatNode" &&
                                            <div className="border border-secondary px-4 py-2" >
                                                <label className="row">Abilty:</label>
                                                <input className="row" value={elementAbility} onChange={(evt) => setElementAbility(Number(evt.target.value))} type="number"></input>
                                                <label className="row">Endurance:</label>
                                                <input className="row" value={elementEndurance} onChange={(evt) => setElementEndurance(Number(evt.target.value))} type="number"></input>
                                            </div>
                                        }
                                        {selected?.type === "enduranceChangeEventNode" &&
                                            <div className="border border-secondary px-4 py-2" >
                                                <label className="row">Endurance Modifier:</label>
                                                <input className="row" value={elementEndurance} onChange={(evt) => setElementEndurance(Number(evt.target.value))} type="number"></input>
                                            </div>
                                        }
                                        {(selected?.type === "scenarioNode" || selected?.type === "departNode" || (selected !== undefined && isEdge(selected))) &&
                                            <div className="border border-secondary px-4 py-2" >
                                                <label className="row">Text:</label>
                                                <textarea className="row" style={{ minHeight: "200px" }} value={elementText} onChange={(evt) => setElementText(evt.target.value)} />
                                            </div>
                                        }
                                        < Form className="border border-secondary border-top-0 px-4 py-2" >
                                            <label className="row">Adding:</label>
                                            <Form.Check
                                                type="radio"
                                                label="Scenario"
                                                checked={typeToAdd === 0}
                                                onChange={() => setTypeToAdd(0)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Combat"
                                                checked={typeToAdd === 1}
                                                onChange={() => setTypeToAdd(1)}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="Endurance Change Event"
                                                checked={typeToAdd === 2}
                                                onChange={() => setTypeToAdd(2)}
                                            />
                                        </Form>
                                    </Panel>
                                </ReactFlow>
                            </ReactFlowProvider>
                            {/* <div className="row">
                            <div className="m-4 px-4 col-10">
                                <label className="row">Summary:</label>
                                <textarea className="row w-100" style={{ minHeight: "200px" }} value={summary} onChange={(evt) => setSummary(evt.target.value)} />
                            </div>
                            <div className="px-4 col d-flex align-items-center">
                                <div className="row">
                                    <button className="btn btn-primary m-2 row" onClick={showMovePlayersPanel}>Players migrations ({playersMigrations.length})</button>
                                    <button className="btn btn-primary m-2 row" onClick={sendStoryToDB}>Save</button>
                                    <button className="btn btn-secondary m-2 row" onClick={loadStory}>Cancel</button>
                                </div>
                            </div>
                        </div> */}
                            <Form onSubmit={sendStoryToDB} noValidate validated={validated} className="row">
                                <div className="m-4 px-4 col-10">
                                    <Form.Group className="form-group mt-3" controlId="validationCustom01">
                                        <Form.Label>Title:</Form.Label>
                                        <Form.Control
                                            required
                                            type="text"
                                            placeholder="Enter title"
                                            minLength={3}
                                            value={title}
                                            onChange={(event) =>
                                                setTitle(event.target.value)
                                            }
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            Please provide a title of longer than 3 letters.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                    <Form.Group className="form-group mt-3" controlId="validationCustom02">
                                        <Form.Label>Summary</Form.Label>
                                        <Form.Control
                                            required
                                            as="textarea"
                                            value={summary}
                                            placeholder="Enter summary"
                                            onChange={(event) =>
                                                setSummary(event.target.value)
                                            }
                                            style={{ minHeight: "200px" }}
                                        />
                                        <Form.Control.Feedback type="invalid">
                                            Please provide a summary.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </div>
                                <div className="px-4 col d-flex align-items-center">
                                    <div className="row">
                                        <button className="btn btn-primary m-2 row" type="button" onClick={showMovePlayersPanel}>Players migrations ({playersMigrations.length})</button>
                                        <button className="btn btn-primary m-2 row" type="submit" disabled={preventSave}>Save</button>
                                        <button className="btn btn-secondary m-2 row" type="button" onClick={loadStory}>Cancel</button>
                                    </div>
                                </div>
                            </Form>
                            <Modal show={showPlayersPanel} onHide={hideMovePlayersPanel}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Moving players</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {playersMigrations.length > 0 && (
                                        <Table striped bordered hover>
                                            <thead>
                                                <tr>
                                                    <td className="align-middle">From</td>
                                                    <td className="align-middle">To</td>
                                                    <td className="align-middle">Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {playersMigrations.map((pm) => (
                                                    <tr>
                                                        <td className="align-middle">{pm.from}</td>
                                                        <td className="align-middle">{pm.to}</td>
                                                        <td className="align-middle">
                                                            <Button type="button" className="btn btn-secondary mx-4" onClick={() => removeMigration(pm.from, pm.to)}>
                                                                Remove
                                                            </Button>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </Table>
                                    )}
                                    <Form onSubmit={handleSubmitMigration}>
                                        <div className="row">
                                            <div className="col">
                                                <label>From</label>
                                                <Form.Select id="from" value={fromForm} onChange={(e) => setFromForm(parseInt(e.target.value))}>
                                                    <option value={-1}>Select chapter</option>
                                                    {
                                                        story?.chapters.map((chapter) => (
                                                            <option value={chapter.id}>{chapter.id}</option>
                                                        ))
                                                    }
                                                </Form.Select>
                                            </div>
                                            <div className="col">
                                                <label>To</label>
                                                <Form.Select id="to" value={toForm} onChange={(event) => setToForm(parseInt(event.target.value))}>
                                                    <option value={-1}>Select chapter</option>
                                                    {
                                                        story?.chapters.map((chapter) => (
                                                            <option value={chapter.id}>{chapter.id}</option>
                                                        ))
                                                    }
                                                </Form.Select>
                                            </div>
                                        </div>
                                        <Button type="submit">OK</Button>
                                    </Form>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button type="button" className="btn btn-secondary mx-4" onClick={hideMovePlayersPanel}>
                                        Ok
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                            <Modal show={helpPanel} onHide={hideHelpPanel}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Help</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <p>
                                        <strong>Nodes:</strong> Need to have one incoming connection (deleting of the last connection is prevented).
                                        If deleted, new connections will be made between the incoming nodes and outgoing nodes of the deleted one.
                                        If selected, will appear in dark purple on the minimap.
                                    </p>
                                    <p>
                                        <strong>Connections:</strong> Can be moved, but only from the source.
                                    </p>
                                    <p>
                                        <strong>Players:</strong> Start with an Ability score of 15 and 25 in Endurance.<br />
                                    </p>
                                    <p>
                                        <strong>Combats:</strong> Should have only one scenario as outcome because the player will be automatically moved in the case of a victory.<br />
                                        <strong>They should be always followed by a scenario node !</strong>
                                    </p>
                                    <p>
                                        <strong>Endurance Modifier Event:</strong> Should have only one scenario as outcome because the player will be automatically moved.<br />
                                        <strong>They should be always followed by a scenario node !</strong>
                                    </p>
                                    <p>
                                        <strong>Errors:</strong> Will appear in red on the minimap and an explication will be displayed above the faulty node. While present, you will be unable to save.
                                    </p>
                                    <p>
                                        <strong>Stats:</strong> Can be displayed using the dedicated button in the controls.
                                    </p>
                                    <p>
                                        <strong>Keybinds:</strong> Backspace is used to delete.
                                    </p>
                                </Modal.Body>
                                <Modal.Footer>
                                    <button type="button" className="btn btn-secondary mx-4" onClick={hideHelpPanel}>
                                        Ok
                                    </button>
                                </Modal.Footer>
                            </Modal>
                        </>
                    }
                </div>
            </main >
        </>);
}