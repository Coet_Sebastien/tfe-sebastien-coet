import { useState, SyntheticEvent } from "react"
import { loginUser, postUser } from "../services/UserService"
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { UserContext } from "./Layout";
import { User } from "../models/user";
import { Form, Button } from "react-bootstrap";

function LoginPage() {
    let [authMode, setAuthMode] = useState("signin")
    let [email, setEmail] = useState('');
    let [password, setPassword] = useState('');
    let [pseudo, setPseudo] = useState('');
    const {
        currentUser,
        setCurrentUser
      } = useContext(UserContext);
    const [error, setError] = useState<string | undefined>(undefined);
    const [validated, setValidated] = useState(false);

    const changeAuthMode = () => {
        setAuthMode(authMode === "signin" ? "signup" : "signin");
        setError('');
        setValidated(false);
    }

    const navigate = useNavigate();

    const navigateHome = () => {
        // 👇️ navigate to /
        navigate('/');
    };

    const handleSubmitLogin = (event: any) => {
        //deal with error
        event.preventDefault();
        setError('');
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
        } else {
            loginUser(email, password)
                .then(logUser)
                .then(navigateHome)
                .catch((error: TypeError) => {
                    //console.log("throwing")
                    setError(error.message);
                });
        }
        setValidated(true);

    };

    async function logUser(user: User) {
        setCurrentUser(user);
        sessionStorage.setItem('user', JSON.stringify(user));
    }

    const handleSubmitPost = (event: any) => {
        //deal with error
        event.preventDefault();
        setError('');
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
        } else {
            postUser(email, password, pseudo)
                .then(() => setAuthMode("signin"))
                .catch((error: TypeError) => {
                    //console.log("throwing")
                    setError(error.message);
                });
        }
        setValidated(true);
    };



    //let buttonTest = () => { console.log(sessionStorage.getItem("User")) };

    if (authMode === "signin") {
        return (
            <main className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen", minHeight: "90vh"}}>
                <div className="row">
                    <div className='col-lg p-4' />
                    <div className='col-lg-8'>
                        <div className='row d-flex justify-content-center'>
                            <Form onSubmit={handleSubmitLogin} noValidate validated={validated} className="col-lg-6 p-4 m-4">
                                <h3 >Log In</h3>
                                <div className="text-center">
                                    Not yet registered?{" "}
                                    <span className="link-primary" onClick={changeAuthMode}>
                                        Sign In
                                    </span>
                                </div>
                                {error && (
                                    <div className="text-danger">
                                        {error}
                                    </div>
                                )}
                                <Form.Group className="form-group mt-3" controlId="validationCustom01">
                                    <Form.Label>Email address:</Form.Label>
                                    <Form.Control
                                        required
                                        type="email"
                                        placeholder="Enter email"
                                        minLength={3}
                                        value={email}
                                        onChange={(event) =>
                                            setEmail(event.target.value)
                                        }
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please provide a valid email adress.
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <Form.Group className="form-group mt-3" controlId="validationCustom02">
                                    <Form.Label>Password:</Form.Label>
                                    <Form.Control
                                        required
                                        type="password"
                                        placeholder="Enter password"
                                        minLength={3}
                                        value={password}
                                        onChange={(event) =>
                                            setPassword(event.target.value)
                                        }
                                    />
                                    <Form.Control.Feedback type="invalid">
                                        Please provide a valid email adress.
                                    </Form.Control.Feedback>
                                </Form.Group>
                                <div className="d-grid gap-2 mt-3">
                                    <Button type="submit" className="my-4">Log in</Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                    <div className='col-lg p-4' />
                </div>
            </main>
        );
    }
    return (
        <main className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen", minHeight: "90vh"}}>
            <div className="row">
                <div className='col-lg p-4' />
                <div className='col-lg-8'>
                    <div className='row d-flex justify-content-center'>
                        <Form onSubmit={handleSubmitPost} noValidate validated={validated} className="col-lg-6 p-4 m-4">
                            <h3>Sign In</h3>
                            <div className="text-center">
                                Already registered?{" "}
                                <span className="link-primary" onClick={changeAuthMode}>
                                    Log In
                                </span>
                            </div>
                            {error && (
                                <div className="text-danger">
                                    {error}
                                </div>
                            )}
                            <Form.Group className="form-group mt-3" controlId="validationCustom01">
                                <Form.Label>Email address:</Form.Label>
                                <Form.Control
                                    required
                                    type="email"
                                    placeholder="Enter email"
                                    minLength={3}
                                    value={email}
                                    onChange={(event) =>
                                        setEmail(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid email adress.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="form-group mt-3" controlId="validationCustom02">
                                <Form.Label>Password:</Form.Label>
                                <Form.Control
                                    required
                                    type="password"
                                    placeholder="Enter password"
                                    minLength={3}
                                    value={password}
                                    onChange={(event) =>
                                        setPassword(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid email adress.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="form-group mt-3" controlId="validationCustom03">
                                <Form.Label>Pseudo:</Form.Label>
                                <Form.Control
                                    required
                                    type="pseudo"
                                    placeholder="e.g George147"
                                    minLength={3}
                                    value={pseudo}
                                    onChange={(event) =>
                                        setPseudo(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid pseudo.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <div className="d-grid gap-2 mt-3">
                                <Button type="submit" className="my-4">Submit</Button>
                            </div>
                        </Form>
                    </div>
                </div>
                <div className='col-lg p-4' />
            </div>
        </main>
    );
}

export default LoginPage;