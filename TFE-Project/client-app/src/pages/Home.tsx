import Card from 'react-bootstrap/Card';
import StoriesTable from '../components/StoriesTable';

function Home() {
    return (
        <div className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen" }}>
            <div className='row'>
                <div className='col-lg p-4 border text-center'>Advertisement here</div>
                <div className='col-lg-8'>
                    <Card className='row p-4 m-4 text-center' bg="light">
                        <Card.Body className='p-4'>
                            <Card.Title className='mb-4'>Welcome to the RPG Creation Tool</Card.Title>
                            <Card.Text className='mx-4'>
                                This is a prototype for a school project. The idea is to give the possibility to users to play an RPG like the Lone Wolf book serie
                                with a discord bot. You can create you own stories with the builder.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <div className='row d-flex justify-content-center p-4'>
                        <Card className='m-4 col-lg-6' bg="light">
                            <Card.Body className='p-4'>
                                <Card.Title className='mb-4 text-center' >How does it works ?</Card.Title>
                                <Card.Subtitle className="text-muted text-center mb-4">As a player:</Card.Subtitle>
                                <Card.Text className='text-left'>
                                    <p>
                                        <a href="https://discord.com/api/oauth2/authorize?client_id=1093514769530630267&permissions=534723951680&scope=bot">Click here to add the bot</a>
                                        <br />
                                        <br />
                                        1) Check if the chatbot is present on your discord server.<br />
                                        2) Browse the available stories.<br />
                                        3) Type /begin followed by the story id that you choose.<br />
                                        4) You're all set !<br />
                                    </p>
                                    <strong>Tip: use a public thread to play for a better experience and cleaner discord server</strong>
                                </Card.Text>
                                <Card.Subtitle className="text-muted text-center mb-4">As a creator:</Card.Subtitle>
                                <Card.Text className='text-left'>
                                    <p>
                                        1) Log in or register.<br />
                                        2) You will have access to your own creations.<br />
                                        3) Use the tool to create new stories for players to try.<br />
                                    </p>
                                    <strong>Tip: if you need help, check the "?" button inside the builder</strong>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    <div className='row m-4 overflow-auto'>
                        <StoriesTable />
                    </div>
                </div>
                <div className='col-lg p-4 border text-center'>Advertisement here</div>
            </div>
        </div>
    )
};

export default Home;
