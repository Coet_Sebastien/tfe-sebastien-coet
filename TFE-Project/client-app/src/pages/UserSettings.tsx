import { useState, SyntheticEvent } from "react"
import { loginUser, postUser } from "../services/UserService"
import { useNavigate } from "react-router-dom";
import { useContext, useEffect } from "react";
import { UserContext } from "./Layout";
import { User } from "../models/user";
import { Form, Button } from "react-bootstrap";
import { putUser } from "../services/UserService";
import { Check } from "react-bootstrap-icons";

function UserSettings() {
    let [password, setPassword] = useState('');
    let [pseudo, setPseudo] = useState('');
    const [errorPassword, setErrorPassword] = useState<string | undefined>(undefined);
    const [errorPseudo, setErrorPseudo] = useState<string | undefined>(undefined);
    const [validated1, setValidated1] = useState(false);
    const [validated2, setValidated2] = useState(false);
    const {
        currentUser,
        setCurrentUser
    } = useContext(UserContext);

    const navigate = useNavigate();

    const handleSubmitPassword = (event: any) => {
        event.preventDefault();
        setErrorPassword('');
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
        } else {
            putUser(password, "")
                .then(() => {
                    setErrorPassword("Saved");
                })
                .catch((error: TypeError) => {
                    //console.log("throwing")
                    setErrorPassword(error.message);
                });
        }
        setValidated2(true);
    };

    const handleSubmitPseudo = (event: any) => {
        event.preventDefault();
        setErrorPseudo('');
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
        } else {
            putUser("", pseudo)
                .then(() => {
                    setErrorPseudo("Saved");
                    setCurrentUser((u: User) => {
                        u.pseudo = pseudo;
                        return u;
                    });
                    if (sessionStorage.user !== undefined) {
                        var tmpUser = JSON.parse(sessionStorage.user);
                        tmpUser.pseudo = pseudo;
                        sessionStorage.user=JSON.stringify(tmpUser);
                    }
                })
                .catch((error: TypeError) => {
                    //console.log("throwing")
                    setErrorPseudo(error.message);
                    setPseudo(currentUser.pseudo)
                });
        }
        setValidated1(true);
    };

    const navigateHome = () => {
        // 👇️ navigate to /
        navigate('/');
    };

    useEffect(() => {
        if (currentUser === undefined && sessionStorage.user === undefined) {
            navigateHome();
        }
    }, [])

    useEffect(() => {
        if (currentUser !== undefined) {
            setPseudo(currentUser.pseudo);
        }
    }, [currentUser])

    useEffect(() => {
        setErrorPassword(undefined);
        setValidated2(false);
    }, [password])

    useEffect(() => {
        setErrorPseudo(undefined);
        setValidated1(false);
    }, [pseudo])


    return (
        <main className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen", minHeight: "90vh" }}>
            <div className="row">
                <div className='col-lg m-4' />
                <div className='col-lg-8'>
                    <div className='row d-flex justify-content-center pt-4'>
                        <h3 className="col-lg-12 text-center pt-4">User settings</h3>
                        <Form onSubmit={handleSubmitPseudo} noValidate validated={validated1} className="px-4 mx-4 col-lg-6" >
                            {errorPseudo && errorPseudo === "Saved" && (
                                <div className="text-success">
                                    <Check /> {errorPseudo}
                                </div>
                            )}
                            {errorPseudo && errorPseudo !== "Saved" && (
                                <div className="text-danger">
                                    {errorPseudo}
                                </div>
                            )}
                            <Form.Group className="form-group mt-3" controlId="validationCustom01">
                                <Form.Label>Change pseudo:</Form.Label>
                                <Form.Control
                                    required
                                    type="pseudo"
                                    placeholder="e.g George147"
                                    minLength={3}
                                    value={pseudo}
                                    onChange={(event) =>
                                        setPseudo(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid pseudo.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button type="submit" className="my-2 float-end">Save</Button>
                        </Form>
                        <Form onSubmit={handleSubmitPassword} noValidate validated={validated2} className="px-4 mx-4 col-lg-6">
                            {errorPassword && errorPassword === "Saved" && (
                                <div className="text-success">
                                    <Check /> {errorPassword}
                                </div>
                            )}
                            {errorPassword && errorPassword !== "Saved" && (
                                <div className="text-danger">
                                    {errorPassword}
                                </div>
                            )}
                            <Form.Group className="form-group mt-3" controlId="validationCustom02">
                                <Form.Label>Change password:</Form.Label>
                                <Form.Control
                                    required
                                    type="password"
                                    placeholder="Enter password"
                                    minLength={3}
                                    value={password}
                                    onChange={(event) =>
                                        setPassword(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid email adress.
                                </Form.Control.Feedback>
                                <Button type="submit" className="my-2 float-end">Save</Button>
                            </Form.Group>
                        </Form>
                    </div>
                </div>
                <div className='col-lg m-4' />
            </div>
        </main>
    );

}

export default UserSettings;