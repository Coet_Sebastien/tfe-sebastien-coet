import { Table } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Story } from "../models/story";
import { User } from "../models/user";
import { getAllStoriesForUser, deleteStory, postStory } from "../services/StoryService";
import { useNavigate } from "react-router-dom";
import { useParams } from 'react-router-dom';
import { Modal, Form, Button } from "react-bootstrap";
import { useContext } from "react";
import { UserContext } from "./Layout";

export function MyStories() {
    const {
        currentUser,
        setCurrentUser
    } = useContext(UserContext);
    const [stories, setStories] = useState<Story[]>([]);
    const params = useParams();
    const columns = [
        { name: "Id", value: "StoryId", propertyName: "storyId" },
        { name: "Title", value: "Title", propertyName: "title" },
        { name: "Summary", value: "Summary", propertyName: "summary" },
        { name: "Total Players", value: "TotalPlayers", propertyName: "totalPlayers" },
        { name: "Timestamp", value: "Timestamp", propertyName: "timestamp" }
    ];
    const navigate = useNavigate();
    const [show, setShow] = useState(false);
    const [storyToDeleteId, setStoryToDeleteId] = useState<number>();
    const [summary, setSummary] = useState("");
    const [title, setTitle] = useState("");
    const [validated, setValidated] = useState(false);
    const [error, setError] = useState<string | undefined>(undefined);

    const navigateHome = () => {
        // 👇️ navigate to /
        navigate('/');
    };

    useEffect(() => {
        if (currentUser === undefined && sessionStorage.user === undefined) {
            navigateHome();
        }
    }, [])

    async function loadStories() {
        if (currentUser != undefined && currentUser.id) {
            await getAllStoriesForUser(currentUser.id).then((data) => {
                setStories(data);
                setTitle("");
                setSummary("");
                setValidated(false);
                console.log(data);
            }).catch(() => {
                setStories([]);
                setTitle("");
                setSummary("");
                setValidated(false);
                console.log("No data");
            });
        } else {
            setStories([]);
            setTitle("");
            setSummary("");
            console.log("No user id");
        }
    }

    useEffect(() => {
        loadStories();
    }, [currentUser]);

    const GetNameOf = (obj: any, propertyName: string) => {
        if (obj.hasOwnProperty(propertyName)) return obj[propertyName];
    };


    const handleClose = () => {
        setShow(false);
        setStoryToDeleteId(undefined);
    };
    const handleCloseAndDelete = () => {
        setError('');
        setShow(false);
        if (storyToDeleteId !== undefined)
            deleteStory(storyToDeleteId, currentUser.id).then(loadStories).catch((error: TypeError) => {
                setError(error.message);
            });
        setStoryToDeleteId(undefined);
    };

    let checkDeleteStory = (storyId: number) => {
        setStoryToDeleteId(storyId);
        setShow(true);
    };

    let navigateToBuilder = (storyId: number) => {
        navigate('/storybuilder/' + storyId);
    };

    const handleSubmitStory = (event: any) => {
        //deal with error
        setError('');
        event.preventDefault();
        console.log(summary, title);
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            console.log("stopped");
            event.preventDefault();
            event.stopPropagation();
        } else {
            console.log("sending");
            if (currentUser !== undefined) {
                postStory(currentUser, summary, title).then(loadStories).catch((error: TypeError) => {
                    setError(error.message);
                });;
            }
        }
        setValidated(true);
    };

    return (
        <div className='h-100 p-4 w-100' style={{ backgroundColor: "lightseagreen", minHeight: "90vh" }}>
            <div className='row h-100' style={{ backgroundColor: "lightseagreen" }}>
                <div className='col-lg p-4 border text-center'>Advertisement here</div>
                <div className='col-lg-8'>
                    <div className="row m-4">
                        {stories.length > 0 && (
                            <div className="overflow-auto">
                                <Table striped bordered hover className="bg-light">
                                    <thead>
                                        <tr>
                                            {columns.map((column) => (
                                                <td className="align-middle" key={column.value}>{column.name}</td>
                                            ))}
                                            <td className="align-middle">Action</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {stories.map((d) => (
                                            <tr key={d.storyId}>
                                                {columns.map((column) => (
                                                    <>
                                                        {d.hasOwnProperty(column.propertyName) &&
                                                            <td className="align-middle" key={"" + d.storyId + "-" + column.propertyName}>{GetNameOf(d, column.propertyName)}</td>
                                                        }
                                                    </>
                                                ))}
                                                <td>
                                                    <div className="btn-group w-100">
                                                        <button type="button" className="btn btn-primary" onClick={(event) => { navigateToBuilder(d.storyId) }}>
                                                            Edit
                                                        </button>
                                                        <button type="button" className="btn btn-secondary" onClick={(event) => { checkDeleteStory(d.storyId) }}>
                                                            Delete
                                                        </button>
                                                    </div>
                                                </td>
                                                {/* To do: delete */}
                                            </tr>
                                        ))}
                                    </tbody>
                                </Table>
                            </div>
                        )}

                        {error && (
                            <div className="text-danger">
                                {error}
                            </div>
                        )}
                        <Form onSubmit={handleSubmitStory} noValidate validated={validated}>
                            <Form.Group className="form-group mt-3" controlId="validationCustom01">
                                <Form.Label>Title:</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    placeholder="Enter title"
                                    minLength={3}
                                    value={title}
                                    onChange={(event) =>
                                        setTitle(event.target.value)
                                    }
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a title of longer than 3 letters.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group className="form-group mt-3" controlId="validationCustom02">
                                <Form.Label>Summary</Form.Label>
                                <Form.Control
                                    required
                                    as="textarea"
                                    value={summary}
                                    placeholder="Enter summary"
                                    onChange={(event) =>
                                        setSummary(event.target.value)
                                    }
                                    style={{ minHeight: "200px" }}
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a summary.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Button type="submit" className="my-4">Submit new story</Button>
                        </Form>
                    </div>
                </div>
                <div className='col-lg p-4 border text-center'>Advertisement here</div>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Deleting</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure to delete the story n°{storyToDeleteId}</Modal.Body>
                <Modal.Footer>
                    <button type="button" className="btn btn-secondary mx-4" onClick={handleCloseAndDelete}>
                        Delete
                    </button>
                    <button type="button" className="btn btn-primary mx-4" onClick={handleClose}>
                        Cancel
                    </button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}