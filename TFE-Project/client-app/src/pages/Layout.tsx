import { useEffect, createContext } from 'react';
import { useState } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { LinkContainer } from 'react-router-bootstrap'
import { Outlet } from "react-router-dom";
import { User } from '../models/user';
import { NavDropdown } from 'react-bootstrap';

export const UserContext = createContext<any>(undefined);

function Layout() {

    const [currentUser, setCurrentUser] = useState<User | null>();

    useEffect(() => {
        if (sessionStorage.user != null) {
            setCurrentUser(JSON.parse(sessionStorage.user))
        }
    }, [])

    let logout = () => {
        setCurrentUser(undefined);
        sessionStorage.removeItem('user');
    }

    return (
        <main style={{ minHeight: "100vh" }} className='h-100 w-100'>
            <Navbar bg="dark" expand="lg" style={{ minHeight: "10vh" }} className='p-4 w-100' variant="dark">
                <LinkContainer to="/">
                    <Navbar.Brand>RPG Creation Tool</Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <LinkContainer to="/">
                            <Nav.Link>Home</Nav.Link>
                        </LinkContainer>
                    </Nav>
                    {currentUser != null &&
                        <>
                            <Nav className="mr-auto ">
                                <LinkContainer to={'mystories/'}>
                                    <Nav.Link>My Stories</Nav.Link>
                                </LinkContainer>
                            </Nav>
                        </>
                    }
                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end">
                    {currentUser != null ? (
                        <Nav>
                            <NavDropdown title={"Signed in as: " + currentUser.pseudo + "  "} id="basic-nav-dropdown" drop="down" align="end" menuVariant='dark'>
                                <LinkContainer to="userSettings">
                                    <NavDropdown.Item className="text-center">
                                        Settings
                                    </NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer to="/">
                                    <NavDropdown.Item onClick={logout} className="text-center">
                                        Log out
                                    </NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                        </Nav>
                    ) : (
                        <Nav className="mr-auto ">
                            <LinkContainer to="login">
                                <Nav.Link>Login</Nav.Link>
                            </LinkContainer>
                        </Nav>
                    )}

                </Navbar.Collapse>
            </Navbar>
            <UserContext.Provider value={{
                currentUser,
                setCurrentUser
            }}>
                <Outlet />
            </UserContext.Provider>
        </main>
    )
};

export default Layout;

