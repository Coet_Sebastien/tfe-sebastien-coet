import { Table } from "react-bootstrap";

interface OrderedByTableProps {
    data: any[],
    columns: { name: string, value: string, propertyName: string, buttonToSort:any}[]
}

function OrderedByTable(props: OrderedByTableProps) {
    const { data, columns} = props;
    if (data.length > 0)
        return (
            <Table striped bordered hover className="bg-light">
                <thead>
                    <tr>
                        {columns.map((column) => (
                            <td key={column.value} onClick={column.buttonToSort}>{column.name}</td>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {data.map((d) => (
                        <tr key={d[columns[0].propertyName]}>
                            {columns.map((column) => (
                                <td key={d[column.propertyName]}>{d[column.propertyName]}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </Table>
        );
    else{
        return <></>;
    }
}

export default OrderedByTable;