import { Node, NodeProps, Handle, Position, HandleProps, NodeToolbar } from 'reactflow';
import { Person } from 'react-bootstrap-icons';

type NodeData = {
    enduranceModifier: number;
    nbCurrentPlayers: number;
    showAlert: boolean;
    stats: any;
};

type EnduranceChangeEventNode = Node<NodeData>;

export function EnduranceChangeEventNode({ data }: NodeProps<NodeData>) {
    return (
        <div className="border border-dark">
            <NodeToolbar isVisible={data.showAlert} position={Position.Top}>
                <div className='text-danger'>Need one connection for victory</div>
            </NodeToolbar>
            {data.stats !== undefined &&
                <NodeToolbar isVisible={data.stats !== undefined} position={Position.Top}>
                    <div className='border border-dark p-1' style={{ backgroundColor: "lightgrey" }}>
                        Total <Person />: {data.stats.totalPlayers}<br />
                        Deaths: {data.stats.deaths}
                    </div>
                </NodeToolbar>
            }
            <Handle
                className='border-dark'
                type="target"
                position={Position.Left}
                isConnectable={true}
                id="a"
            />
            {data.stats !== undefined &&
                <div className='text-center border-bottom border-dark'>
                    ID: {data.stats.id}
                </div>
            }
            <div className='px-3 py-2'>
                <div>Modifier: {data.enduranceModifier} E</div>
            </div>
            <div className="text-center border-top border-dark px-4 py-1">
                <Person />: {data.nbCurrentPlayers}
            </div>
            <Handle
                className='border-dark'
                type="source"
                position={Position.Right}
                isConnectable={true}
                id="a"
            />
        </div>);
}