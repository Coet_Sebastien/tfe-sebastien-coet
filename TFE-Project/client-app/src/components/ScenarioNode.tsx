import { Node, NodeProps, Handle, Position, HandleProps, NodeToolbar } from 'reactflow';
import { Person } from 'react-bootstrap-icons';

type NodeData = {
    label: string;
    nbCurrentPlayers: number;
    stats: any;
};
//style={{ backgroundColor: "lightgreen" }}

type ScenarioNode = Node<NodeData>;

export function ScenarioNode({ data }: NodeProps<NodeData>) {
    return (
        <div className="border border-dark">
            {data.stats !== undefined &&
                <NodeToolbar isVisible={data.stats !== undefined} position={Position.Top}>
                    <div className='border border-dark p-1' style={{ backgroundColor: "lightgrey" }}>
                        Total <Person />: {data.stats.totalPlayers}<br />
                        Quits: {data.stats.quits}
                    </div>
                </NodeToolbar>
            }
            <Handle
                className='border-dark'
                type="target"
                position={Position.Left}
                isConnectable={true}
                id="a"
            />
            {data.stats !== undefined &&
                <div className='text-center border-bottom border-dark'>
                    ID: {data.stats.id}
                </div>
            }
            <div className='px-4 py-2'>
                <span className="text-break">
                    {data.label}
                </span>
            </div>
            <div className="text-center border-top border-dark px-4 py-1">
                <Person />: {data.nbCurrentPlayers}
            </div>
            <Handle
                className='border-dark'
                type="source"
                position={Position.Right}
                isConnectable={true}
                id="a"
            />
        </div>);
}