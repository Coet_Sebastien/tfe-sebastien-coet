import { useState, useEffect } from 'react';
import { getAllStories } from '../services/StoryService';
import { Story } from '../models/story';
import OrderedByTable from './OrderedByTable';

function StoriesTable() {
    const [stories, setStories] = useState<Story[]>([]);
    const [currentPropertyName, setCurrentPropertyName] = useState<string>("storyId")

    useEffect(() => {
        async function loadStories() {
            const data = await getAllStories();
            setStories(data);
        }
        loadStories();
    }, []);

    function sortStoriesBy(propertyName: string) {
        if (currentPropertyName === propertyName && propertyName !== "") {
            var tmp = [...stories].reverse();
            setStories(tmp);
        } else {
            if (propertyName === "storyId"
                || propertyName === "authorName"
                || propertyName === "title"
                || propertyName === "totalPlayers"
                || propertyName === "timestamp") {
                setCurrentPropertyName(propertyName);
                var tmp = [...stories].sort((n1: Story, n2: Story) => {
                    if (n1[propertyName] > n2[propertyName]) {
                        return 1;
                    }

                    if (n1[propertyName] < n2[propertyName]) {
                        return -1;
                    }
                    return 0;
                })
                setStories(tmp);
            }
        }
    }

    //const buttonToSort = () => sortStoriesBy("storyId");

    //Add sortStoriesBy("storyId") to columns
    const columns = [
        { name: "Id", value: "StoryId", propertyName: "storyId", buttonToSort:()=> sortStoriesBy("storyId")},
        { name: "Author", value: "Author.Pseudo", propertyName: "authorName", buttonToSort:()=> sortStoriesBy("authorName") },
        { name: "Title", value: "Title", propertyName: "title", buttonToSort:()=>sortStoriesBy("title")},
        { name: "Summary", value: "Summary", propertyName: "summary", buttonToSort:()=>sortStoriesBy("")},
        { name: "Total Players", value: "TotalPlayers", propertyName: "totalPlayers", buttonToSort:()=> sortStoriesBy("totalPlayers") },
        { name: "Timestamp", value: "Timestamp", propertyName: "timestamp", buttonToSort:()=> sortStoriesBy("timestamp") }
    ];

    if (stories.length > 0)
        return (
            <OrderedByTable data={stories} columns={columns} />
        );
    return <></>;
}

export default StoriesTable;