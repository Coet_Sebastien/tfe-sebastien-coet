export enum Role {
    Member = 0,
    Manager = 1,
    Admin = 2
}

export class User {
    
    id: number | undefined;
    pseudo: string = '';
    password: string = '';
    email: string = '';
    token: string = '';
    role: Role = Role.Member;

    constructor(initializer?: any) {
        if (!initializer) return;
        if (initializer.id) this.id = initializer.id;
        if (initializer.pseudo) this.pseudo = initializer.pseudo;
        if (initializer.password) this.password = initializer.password;
        if (initializer.email) this.email = initializer.email;
        if (initializer.token) this.token = initializer.token;
        if (initializer.role) this.role = initializer.role;
    }

    public get roleAsString(): string {
        return Role[this.role];
    }

}
