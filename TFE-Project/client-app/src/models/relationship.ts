export class Relationship {
    body: string="";
    input: number=0;
    output: number=0;
    storyId: number=0

    constructor(initializer?: any) {
        if (!initializer) return;
        if (initializer.body) this.body = initializer.body;
        if (initializer.input) this.input = initializer.input;
        if (initializer.output) this.output = initializer.output;
        if (initializer.storyId) this.storyId = initializer.storyId;
    }
}