import { Chapter } from "../models/chapter";
import { Relationship } from "../models/relationship";
import { User } from "./user";

export class Story {

    author: User = new User;
    beginningChapterId: number = 0;
    chapters: Chapter[] = [];
    totalPlayers: number = 0;
    relationships: Relationship[] = [];
    storyId: number = 0;
    title: string = "";
    summary: string = "";
    timestamp: string = "";
    authorName: string = "";

    constructor(initializer?: any) {
        if (!initializer) return;
        if (initializer.author){
            this.author = initializer.author;
            this.authorName = initializer.author.pseudo
        } 
        if (initializer.beginningChapterId) this.beginningChapterId = initializer.beginningChapterId;
        if (initializer.chapters) this.chapters = initializer.chapters;
        if (initializer.totalPlayers) this.totalPlayers = initializer.totalPlayers;
        if (initializer.relationships) this.relationships = initializer.relationships;
        if (initializer.storyId) this.storyId = initializer.storyId;
        if (initializer.summary) this.summary = initializer.summary;
        if (initializer.title) this.title = initializer.title;
        if (initializer.timestamp) this.timestamp = initializer.timestamp &&
            initializer.timestamp.length > 10 ? initializer.timestamp.substring(0, 10) : initializer.timestamp;
    }
}