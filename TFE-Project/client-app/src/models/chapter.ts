import { Relationship } from "./relationship";

export class Chapter {

    id: number=0;
    incomingRelationships: Relationship[] = [];
    nbCurrentPlayers: number=0;
    outgoingRelationships: Relationship[] = [];
    storyId: number=0;
    x:number=0;
    y:number=0;
    type: string="";
    //specific to Scenario
    body: string="";
    victory:Relationship|null=null
    victoryId:number=0;
    ability:number=0;
    endurance:number=0;
    enduranceModifier:number=0;

    constructor(initializer?: any) { 
        if (!initializer) return;
        if (initializer.body) this.body = initializer.body;
        if (initializer.id) this.id = initializer.id;
        if (initializer.chapters) this.incomingRelationships = initializer.incomingRelationships;
        if (initializer.nbCurrentPlayers) this.nbCurrentPlayers = initializer.nbCurrentPlayers;
        if (initializer.outgoingRelationships) this.outgoingRelationships = initializer.outgoingRelationships;
        if (initializer.storyId) this.storyId = initializer.storyId;
        if (initializer.x) this.x = initializer.x;
        if (initializer.y) this.y = initializer.y;
        if (initializer.type) this.type = initializer.type;
        if (initializer.victory) this.victory = initializer.victory;
        if (initializer.victoryId) this.victoryId = initializer.victoryId;
        if (initializer.ability) this.ability = initializer.ability;
        if (initializer.endurance) this.endurance = initializer.endurance;
        if (initializer.endurance) this.endurance = initializer.enduranceModifier;
    }
}