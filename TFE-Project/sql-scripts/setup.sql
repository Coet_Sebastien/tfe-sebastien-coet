USE tfe-db;
CREATE TABLE `tfe-db.Chapters` (
  `Id` int(11) NOT NULL,
  `StoryId` int(11) NOT NULL,
  `X` int(11) NOT NULL,
  `Y` int(11) NOT NULL,
  `Type` longtext NOT NULL,
  `Ability` int(11) DEFAULT NULL,
  `Endurance` int(11) DEFAULT NULL,
  `Body` longtext,
  `VictoryId` int(11) DEFAULT NULL,
  `EnduranceChangeEvent_VictoryId` int(11) DEFAULT NULL,
  `EnduranceModifier` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `tfe-db.Chapters`
--

INSERT INTO `tfe-db.Chapters` (`Id`, `StoryId`, `X`, `Y`, `Type`, `Ability`, `Endurance`, `Body`, `VictoryId`, `EnduranceChangeEvent_VictoryId`, `EnduranceModifier`) VALUES
(5, 3, 0, 0, 'Scenario', NULL, NULL, 'LoreIpsum Chapitre 5', NULL, NULL, NULL),
(6, 4, 0, 0, 'Scenario', NULL, NULL, 'LoreIpsum Chapitre 6', NULL, NULL, NULL),
(23, 5, 35, 14, 'Scenario', NULL, NULL, 'Lore iadadadaagffgsqklsfgqklqfgqklfgqlkfgqslfkgqflgqfgqlfgqlkfgqfgqlfgqlgffqlgfqqlfqgqfgqlfgqlfgqfgqlfgqlfgqflqfgfqlqfgqflfgqlfgqlflqfgqflfqgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', NULL, NULL, NULL),
(24, 5, 431, 46, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(25, 5, 406, 307, 'Combat', 10, 20, NULL, 27, NULL, NULL),
(26, 5, 1227, 34, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(27, 5, 733, 197, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(29, 5, 1042, 268, 'Combat', 2, 2, NULL, 35, NULL, NULL),
(35, 5, 1414, 287, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(36, 5, 1697, 311, 'Combat', 40, 40, NULL, 37, NULL, NULL),
(37, 5, 2051, 331, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(40, 5, 1597, 30, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(42, 5, 2056, -33, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(43, 5, 1861, 133, 'Combat', 0, 0, NULL, 42, NULL, NULL),
(46, 5, 858, -106, 'EnduranceChangeEvent', NULL, NULL, NULL, NULL, 26, -6),
(47, 5, 1415, -72, 'EnduranceChangeEvent', NULL, NULL, NULL, NULL, 40, 7),
(48, 5, 2265, -36, 'Scenario', NULL, NULL, 'New node', NULL, NULL, NULL),
(49, 6, 0, 0, 'Scenario', NULL, NULL, 'New', NULL, NULL, NULL),
(50, 5, 2261, 347, 'Scenario', NULL, NULL, 'END', NULL, NULL, NULL),
(51, 7, 0, 0, 'Scenario', NULL, NULL, 'New', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tfe-db.Playerlogs`
--

CREATE TABLE `tfe-db.Playerlogs` (
  `Id` int(11) NOT NULL,
  `DiscordId` longtext NOT NULL,
  `ChapterId` int(11) NOT NULL,
  `OldChapterId` int(11) DEFAULT NULL,
  `StoryId` int(11) NOT NULL,
  `EnduranceLeft` int(11) NOT NULL,
  `Action` longtext NOT NULL,
  `Timestamp` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `tfe-db.Playerlogs`
--

INSERT INTO `tfe-db.Playerlogs` (`Id`, `DiscordId`, `ChapterId`, `OldChapterId`, `StoryId`, `EnduranceLeft`, `Action`, `Timestamp`) VALUES
(54, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-01 16:24:04.729648'),
(55, '215462241745502208', 25, 23, 5, 25, 'MOVE', '2023-05-01 16:24:13.355514'),
(56, '215462241745502208', 27, 25, 5, 23, 'MOVE', '2023-05-01 16:24:13.643736'),
(57, '215462241745502208', 29, 27, 5, 23, 'MOVE', '2023-05-01 16:24:21.996950'),
(58, '215462241745502208', 35, 29, 5, 22, 'MOVE', '2023-05-01 16:24:22.215618'),
(59, '215462241745502208', 36, 35, 5, 22, 'MOVE', '2023-05-01 16:24:31.113402'),
(60, '215462241745502208', 36, NULL, 5, 22, 'DIED', '2023-05-01 16:24:31.379571'),
(61, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-01 16:24:39.641611'),
(62, '215462241745502208', 24, 23, 5, 25, 'MOVE', '2023-05-01 16:24:47.911800'),
(63, '215462241745502208', 46, 24, 5, 25, 'MOVE', '2023-05-01 16:24:53.925225'),
(64, '215462241745502208', 26, 46, 5, 19, 'MOVE', '2023-05-01 16:24:54.196653'),
(65, '215462241745502208', 47, 26, 5, 19, 'MOVE', '2023-05-01 16:25:10.406652'),
(66, '215462241745502208', 40, 47, 5, 25, 'MOVE', '2023-05-01 16:25:10.639983'),
(67, '215462241745502208', 42, 40, 5, 25, 'MOVE', '2023-05-01 16:25:17.481281'),
(68, '215462241745502208', 48, 42, 5, 25, 'MOVE', '2023-05-01 16:25:21.932092'),
(69, '215462241745502208', 48, NULL, 5, 25, 'END', '2023-05-01 16:25:21.942255'),
(70, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-01 16:25:29.082912'),
(71, '215462241745502208', 25, 23, 5, 25, 'MOVE', '2023-05-01 16:25:36.504267'),
(72, '215462241745502208', 27, 25, 5, 20, 'MOVE', '2023-05-01 16:25:36.824889'),
(73, '215462241745502208', 27, NULL, 5, 20, 'QUIT', '2023-05-01 16:25:41.166745'),
(74, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 15:37:05.965865'),
(75, '215462241745502208', 24, 23, 5, 25, 'MOVE FROM Scenario', '2023-05-03 15:37:14.048721'),
(76, '215462241745502208', 24, NULL, 5, 25, 'QUIT', '2023-05-03 15:37:20.164550'),
(77, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 15:39:42.236211'),
(78, '215462241745502208', 23, NULL, 5, 25, 'QUIT', '2023-05-03 15:39:47.093851'),
(79, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 15:49:23.420623'),
(80, '215462241745502208', 23, NULL, 5, 25, 'QUIT', '2023-05-03 15:49:31.662322'),
(81, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 15:56:27.787511'),
(82, '215462241745502208', 23, NULL, 5, 25, 'QUIT', '2023-05-03 15:56:33.078987'),
(83, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 16:03:27.503817'),
(84, '215462241745502208', 24, 23, 5, 25, 'MOVE FROM Scenario', '2023-05-03 16:03:45.506586'),
(85, '215462241745502208', 24, NULL, 5, 25, 'QUIT', '2023-05-03 16:03:49.921111'),
(86, '215462241745502208', 23, NULL, 5, 25, 'CREATED', '2023-05-03 16:06:21.361614'),
(87, '215462241745502208', 25, 23, 5, 25, 'MOVE FROM Scenario', '2023-05-03 16:06:29.348116'),
(88, '215462241745502208', 27, 25, 5, 19, 'MOVE FROM Combat', '2023-05-03 16:06:29.704005'),
(89, '215462241745502208', 26, 27, 5, 19, 'MOVE FROM Scenario', '2023-05-03 16:06:38.159227'),
(90, '215462241745502208', 26, NULL, 5, 19, 'QUIT', '2023-05-03 16:06:41.788734');

-- --------------------------------------------------------

--
-- Structure de la table `tfe-db.Players`
--

CREATE TABLE `tfe-db.Players` (
  `Id` int(11) NOT NULL,
  `DiscordId` varchar(255) NOT NULL,
  `ChapterId` int(11) NOT NULL,
  `Ability` int(11) NOT NULL DEFAULT '0',
  `Endurance` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tfe-db.Relationships`
--

CREATE TABLE `tfe-db.Relationships` (
  `Input` int(11) NOT NULL,
  `Output` int(11) NOT NULL,
  `Body` longtext NOT NULL,
  `StoryId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `tfe-db.Relationships`
--

INSERT INTO `tfe-db.Relationships` (`Input`, `Output`, `Body`, `StoryId`) VALUES
(23, 24, 'Fuir', 5),
(23, 25, 'Combattre', 5),
(24, 26, 'New option', 5),
(24, 46, 'It\s a trap !', 5),
(25, 27, 'Victoire', 5),
(26, 40, 'New option', 5),
(26, 47, 'heal', 5),
(27, 26, 'Fuir', 5),
(27, 29, 'Combattre', 5),
(29, 35, 'Victoire', 5),
(35, 36, 'New option', 5),
(36, 37, 'New option', 5),
(37, 50, 'New option', 5),
(40, 42, 'New option', 5),
(40, 43, 'New option', 5),
(42, 48, 'New option', 5),
(43, 42, 'New option', 5),
(46, 26, 'New option', 5),
(47, 40, 'New option', 5);

-- --------------------------------------------------------

--
-- Structure de la table `tfe-db.Stories`
--

CREATE TABLE `tfe-db.Stories` (
  `StoryId` int(11) NOT NULL,
  `AuthorId` int(11) DEFAULT NULL,
  `Title` longtext NOT NULL,
  `Summary` longtext NOT NULL,
  `Timestamp` datetime(6) NOT NULL,
  `BeginningChapterId` int(11) NOT NULL,
  `Totaltfe-db.Players` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `tfe-db.Stories`
--

INSERT INTO `tfe-db.Stories` (`StoryId`, `AuthorId`, `Title`, `Summary`, `Timestamp`, `BeginningChapterId`, `Totaltfe-db.Players`) VALUES
(3, NULL, 'Story 3', 'test 1', '2019-11-15 08:30:00.000000', 5, 0),
(4, NULL, 'Story 4', 'test 1', '2023-04-12 19:50:13.868686', 6, 0),
(5, 1, 'OUY', 'dadadada', '2023-04-14 16:27:51.527691', 23, 8),
(6, 1, 'adadad', 'adadadad', '2023-05-01 17:38:55.057142', 49, 0),
(7, 1, 'daadaa', 'fafafafaf', '2023-05-03 15:41:39.367564', 51, 0);

-- --------------------------------------------------------

--
-- Structure de la table `tfe-db.Users`
--

CREATE TABLE `tfe-db.Users` (
  `Id` int(11) NOT NULL,
  `Pseudo` varchar(255) NOT NULL,
  `Password` longtext NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `tfe-db.Users`
--

INSERT INTO `tfe-db.Users` (`Id`, `Pseudo`, `Password`, `Email`, `Role`) VALUES
(1, 'ben1', 'km1LmgBTKE3XJc1iUwBitw4zNIYCYfDIzYkK0O8BbGU=', 'ben@ours', 1),
(3, 'bot', 'gYnsPoqYauvr4wh+d1whXdMHg19GTQeoh9pCr36AXz8=', 'bot@bot', 1),
(4, 'roger323', '50Pj0F3OPFIUQz69V5e5KHRHMAyvC8b6K8OM7v33AZE=', 'oncle-roger@hotmail.be', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `tfe-db.Chapters`
--
ALTER TABLE `tfe-db.Chapters`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_tfe-db.Chapters_StoryId` (`StoryId`);

--
-- Index pour la table `tfe-db.Playerlogs`
--
ALTER TABLE `tfe-db.Playerlogs`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_tfe-db.Playerlogs_ChapterId` (`ChapterId`),
  ADD KEY `IX_tfe-db.Playerlogs_StoryId` (`StoryId`),
  ADD KEY `IX_tfe-db.Playerlogs_OldChapterId` (`OldChapterId`);

--
-- Index pour la table `tfe-db.Players`
--
ALTER TABLE `tfe-db.Players`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_tfe-db.Players_DiscordId` (`DiscordId`),
  ADD KEY `IX_tfe-db.Players_ChapterId` (`ChapterId`);

--
-- Index pour la table `tfe-db.Relationships`
--
ALTER TABLE `tfe-db.Relationships`
  ADD PRIMARY KEY (`Input`,`Output`),
  ADD KEY `IX_tfe-db.Relationships_Output` (`Output`),
  ADD KEY `IX_tfe-db.Relationships_StoryId` (`StoryId`);

--
-- Index pour la table `tfe-db.Stories`
--
ALTER TABLE `tfe-db.Stories`
  ADD PRIMARY KEY (`StoryId`),
  ADD KEY `IX_tfe-db.Stories_AuthorId` (`AuthorId`);

--
-- Index pour la table `tfe-db.Users`
--
ALTER TABLE `tfe-db.Users`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `IX_tfe-db.Users_Pseudo_Email` (`Pseudo`,`Email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `tfe-db.Chapters`
--
ALTER TABLE `tfe-db.Chapters`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT pour la table `tfe-db.Playerlogs`
--
ALTER TABLE `tfe-db.Playerlogs`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT pour la table `tfe-db.Players`
--
ALTER TABLE `tfe-db.Players`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tfe-db.Stories`
--
ALTER TABLE `tfe-db.Stories`
  MODIFY `StoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `tfe-db.Users`
--
ALTER TABLE `tfe-db.Users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `tfe-db.Chapters`
--
ALTER TABLE `tfe-db.Chapters`
  ADD CONSTRAINT `FK_tfe-db.Chapters_tfe-db.Stories_StoryId` FOREIGN KEY (`StoryId`) REFERENCES `tfe-db.Stories` (`StoryId`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tfe-db.Playerlogs`
--
ALTER TABLE `tfe-db.Playerlogs`
  ADD CONSTRAINT `FK_tfe-db.Playerlogs_tfe-db.Chapters_ChapterId` FOREIGN KEY (`ChapterId`) REFERENCES `tfe-db.Chapters` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_tfe-db.Playerlogs_tfe-db.Chapters_OldChapterId` FOREIGN KEY (`OldChapterId`) REFERENCES `tfe-db.Chapters` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_tfe-db.Playerlogs_tfe-db.Stories_StoryId` FOREIGN KEY (`StoryId`) REFERENCES `tfe-db.Stories` (`StoryId`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tfe-db.Players`
--
ALTER TABLE `tfe-db.Players`
  ADD CONSTRAINT `FK_tfe-db.Players_tfe-db.Chapters_ChapterId` FOREIGN KEY (`ChapterId`) REFERENCES `tfe-db.Chapters` (`Id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tfe-db.Relationships`
--
ALTER TABLE `tfe-db.Relationships`
  ADD CONSTRAINT `FK_tfe-db.Relationships_tfe-db.Chapters_Input` FOREIGN KEY (`Input`) REFERENCES `tfe-db.Chapters` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_tfe-db.Relationships_tfe-db.Chapters_Output` FOREIGN KEY (`Output`) REFERENCES `tfe-db.Chapters` (`Id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_tfe-db.Relationships_tfe-db.Stories_StoryId` FOREIGN KEY (`StoryId`) REFERENCES `tfe-db.Stories` (`StoryId`) ON DELETE CASCADE;

--
-- Contraintes pour la table `tfe-db.Stories`
--
ALTER TABLE `tfe-db.Stories`
  ADD CONSTRAINT `FK_tfe-db.Stories_tfe-db.Users_AuthorId` FOREIGN KEY (`AuthorId`) REFERENCES `tfe-db.Users` (`Id`) ON DELETE SET NULL;
