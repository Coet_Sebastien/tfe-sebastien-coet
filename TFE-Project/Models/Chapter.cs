﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public abstract class Chapter
    {
        [Key]
        public int Id { get; set; }
        public int StoryId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public abstract string Type { get; set; }
        public virtual Story Story { get; set; }
        public virtual IList<Relationship> IncomingRelationships { get; set; }
        public virtual IList<Relationship> OutgoingRelationships { get; set; }
        public virtual IList<Player> CurrentPlayers { get; set; }
    }
}
