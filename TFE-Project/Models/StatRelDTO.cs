using Microsoft.Extensions.Hosting;
using System;

namespace TFE_Project.Models
{
    public class StatRelDTO
    {
        public int Input { get; set; }
        public int Output { get; set; }
        public int TotalPlayers { get; set; }
    }
}
