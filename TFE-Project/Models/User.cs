﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;

namespace TFE_Project.Models
{
    public class User : IValidatableObject
    {
        [Key]
        public int Id { get; set; }

        [MinLength(3, ErrorMessage = "Minimum 3 characters")]
        public string Pseudo { get; set; }

        [Required(ErrorMessage = "Required")]
        [MinLength(3, ErrorMessage = "Minimum 3 characters")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [MinLength(3, ErrorMessage = "Minimum 3 characters")]
        public string Email { get; set; }

        public Role Role { get; set; } = Role.Member;
        //public virtual IList<Story> Creations { get; set; } = new List<Story>();
        [NotMapped]
        public String Token { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            var currContext = validationContext.GetService(typeof(DbContext));

            Debug.Assert(currContext != null);

            if (Password == "abc")

                yield return new ValidationResult("The password may not be equal to 'abc'", new[] { nameof(Password) });

        }
    }

    public enum Role
    {
        Admin = 1, Member = 0
    }
}
