﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TFE_Project.Models
{
    public class ChapterDTO
    {
        public int Id { get; set; }
        public int StoryId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int NbCurrentPlayers { get; set; }
        public string Type {get;set;}
        //Scenario
        public string? Body { get; set; }
        //Scenario
        public List<RelationshipDTO>? OutgoingRelationships { get; set; }
        //Combat 
        //Scenario
        //EnduranceChangeEvent
        public List<RelationshipDTO>? IncomingRelationships { get; set; }
        //Combat
        public RelationshipDTO? Victory { get; set; }
        //Combat
        public int? Ability { get; set; }
        //Combat
        public int? Endurance { get; set; }
        //Combat
        public int? VictoryId {get;set;}
        //EnduranceChangeEvent
        public int? EnduranceModifier { get; set; }
    }
}
