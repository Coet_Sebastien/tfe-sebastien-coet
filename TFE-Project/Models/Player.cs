﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public class Player
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string DiscordId { get; set; }
        public int ChapterId { get; set; }
        public virtual Chapter CurrentChapter { get; set; }
        public int Ability { get; set; }
        public int Endurance { get; set; }
    }
}
