﻿namespace TFE_Project.Models
{
    public class RelationshipDTO
    {
        public int Input { get; set; }
        public int Output { get; set; }
        public string Body { get; set; }
        public int StoryId { get; set; }
    }
}
