﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;

namespace TFE_Project.Models
{
    public class Story
    {
        [Key]
        public int StoryId { get; set; }
        public int? AuthorId { get; set; }
        public virtual User? Author { get; set; }
        [Required(ErrorMessage = "Required")]
        [MinLength(3, ErrorMessage = "Minimum 3 characters")]
        public string Title { get; set; }
        [Required]
        public string Summary { get; set; }
        public DateTime Timestamp { get; set; } = DateTime.Now;
        public virtual IList<Chapter> Chapters { get; set; } = new List<Chapter>();
        public virtual IList<Relationship> Relationships { get; set; } = new List<Relationship>();
        public int BeginningChapterId { get; set; }
        public int TotalPlayers { get; set; } = 0;
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var currContext = validationContext.GetService(typeof(DbContext));
            Debug.Assert(currContext != null);
            if (Summary == null)
                yield return new ValidationResult("The Summary can't be empty.", new[] { nameof(Summary) });
                if (Title == null)
                yield return new ValidationResult("The Title can't be empty.", new[] { nameof(Title) });
        }
    }
}
