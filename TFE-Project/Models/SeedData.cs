﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Azure;
using Microsoft.Extensions.Hosting;
using TFE_Project.Helpers;

namespace TFE_Project.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new TFEContext(serviceProvider.GetRequiredService<DbContextOptions<TFEContext>>()))
            {
                try
                {

                    Console.Write("Seeding data ...");

                    if (context.Users.Count() == 0)
                    {
                        context.Users.AddRange(
                            new User() { Id = 1, Pseudo = "ben", Password = TokenHelper.GetPasswordHash("ben"), Email = "ben@ours.com", Role = Role.Admin },
                            new User() { Id = 2, Pseudo = "bruno", Password = TokenHelper.GetPasswordHash("bruno"), Email = "bruon@ours.com" },
                            new User() { Id = 3, Pseudo = "bot", Password = TokenHelper.GetPasswordHash("bot"), Email = "bot@bot.com", Role = Role.Admin },
                            new User() { Id = 4, Pseudo = "profs", Password = TokenHelper.GetPasswordHash("profs"), Email = "profs@test.com", Role = Role.Admin }
                        );

                        context.SaveChanges();

                    }

                    if (context.Stories.Count() == 0)
                    {

                        context.Stories.AddRange(
                            new Story() { StoryId = 1, AuthorId = 1,Title = "Story 1", Summary = "test 1", BeginningChapterId = 1, Timestamp = new DateTime(2019, 11, 15, 8, 30, 0) },
                            new Story() { StoryId = 2, AuthorId = 1,Title = "Story 2", Summary = "test 1", BeginningChapterId = 4, Timestamp = new DateTime(2019, 11, 15, 8, 30, 0)  },
                            new Story() { StoryId = 3, AuthorId = 2, Title = "Story 3", Summary = "test 1", BeginningChapterId = 5, Timestamp = new DateTime(2019, 11, 15, 8, 30, 0)  },
                            new Story() { StoryId = 4, Title = "Story 4", Summary = "test 1", BeginningChapterId = 6 }
                        );

                        context.SaveChanges();

                    }

                    if (context.Chapters.Count() == 0)
                    {

                        context.Chapters.AddRange(
                            new Scenario() 
                            {
                                Id = 1, 
                                Body = @"LoreIpsum Chapitre 1", 
                                StoryId = 1,
                                X=0,
                                Y=0
                            },
                            new Scenario() 
                            {
                                Id = 2, 
                                Body = @"LoreIpsum Chapitre 1", 
                                StoryId = 2,
                                X=0,
                                Y=0
                            },
                            new Scenario() 
                            {
                                Id = 3, 
                                Body = @"LoreIpsum Chapitre 1", 
                                StoryId = 3,
                                X=0,
                                Y=0
                            },
                            new Scenario() 
                            {
                                Id = 4, 
                                Body = @"LoreIpsum Chapitre 1", 
                                StoryId = 4,
                                X=0,
                                Y=0
                            }
                        );

                        context.SaveChanges();

                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.ToString());

                }

            }

        }

    }

}

