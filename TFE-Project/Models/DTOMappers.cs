﻿using Castle.Components.DictionaryAdapter.Xml;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TFE_Project.Models
{
    public static class DTOMappers
    {
        public static UserDTO ToDTO(this User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                Pseudo = user.Pseudo,
                // we don't put the password in the DTO for security reasons
                Password = "",
                Email = user.Email,
                Role = user.Role,
                //Creations = user.Creations.ToDTO()
            };
        }

        public static List<UserDTO> ToDTO(this IEnumerable<User> users)
        {
            return users.Select(u => u.ToDTO()).ToList();
        }

        public static StoryDTO ToDTO(this Story story)
        {
            var tmp = new StoryDTO { };
            if (story.Author == null)
            {
                var fakeAuthor = new UserDTO
                {
                    Id = 0,
                    Pseudo = "[deleted]",
                    Email = "",
                    Role = Role.Member
                };
                tmp.Author = fakeAuthor;
            }
            else
            {
                tmp.Author = story.Author.ToDTO();
            }
            tmp.Title = story.Title;
            tmp.StoryId = story.StoryId;
            tmp.Summary = story.Summary;
            tmp.Timestamp = story.Timestamp;
            tmp.BeginningChapterId = story.BeginningChapterId;
            tmp.Chapters = story.Chapters.ToDTO();
            tmp.TotalPlayers = story.TotalPlayers;
            tmp.Relationships = story.Relationships.ToDTO();
            return tmp;
        }

        public static List<StoryDTO> ToDTO(this IEnumerable<Story> stories)
        {
            return stories.Select(u => u.ToDTO()).ToList();
        }

        public static ChapterDTO ToDTO(this Chapter chapter)
        {
            switch (chapter.Type)
            {
                case "Combat":
                    Combat combat = (Combat)chapter;
                    return new ChapterDTO
                    {
                        Id = combat.Id,
                        StoryId = combat.StoryId,
                        X = combat.X,
                        Y = combat.Y,
                        Type = combat.Type,
                        NbCurrentPlayers = combat.CurrentPlayers.Count(),
                        //specific
                        IncomingRelationships = combat.IncomingRelationships.ToDTO(),
                        VictoryId = combat.VictoryId,
                        Victory = (combat.OutgoingRelationships.SingleOrDefault(r => r.Input == combat.Id && r.Output == combat.VictoryId)).ToDTO(),
                        Ability = combat.Ability,
                        Endurance = combat.Endurance,
                    };
                case "EnduranceChangeEvent":
                    EnduranceChangeEvent enduranceChangeEvent = (EnduranceChangeEvent)chapter;
                    return new ChapterDTO
                    {
                        Id = enduranceChangeEvent.Id,
                        StoryId = enduranceChangeEvent.StoryId,
                        X = enduranceChangeEvent.X,
                        Y = enduranceChangeEvent.Y,
                        Type = enduranceChangeEvent.Type,
                        NbCurrentPlayers = enduranceChangeEvent.CurrentPlayers.Count(),
                        //specific
                        IncomingRelationships = enduranceChangeEvent.IncomingRelationships.ToDTO(),
                        VictoryId = enduranceChangeEvent.VictoryId,
                        Victory = (enduranceChangeEvent.OutgoingRelationships.SingleOrDefault(r => r.Input == enduranceChangeEvent.Id && r.Output == enduranceChangeEvent.VictoryId)).ToDTO(),
                        EnduranceModifier = enduranceChangeEvent.EnduranceModifier,
                    };
                case "Scenario":
                    Scenario scenario = (Scenario)chapter;
                    return new ChapterDTO
                    {
                        Id = scenario.Id,
                        StoryId = scenario.StoryId,
                        X = scenario.X,
                        Y = scenario.Y,
                        Type = scenario.Type,
                        NbCurrentPlayers = scenario.CurrentPlayers.Count(),
                        //specific
                        IncomingRelationships = scenario.IncomingRelationships.ToDTO(),
                        OutgoingRelationships = scenario.OutgoingRelationships.ToDTO(),
                        Body = scenario.Body
                    };
                default:
                    return null;
            }
        }

        public static List<ChapterDTO> ToDTO(this IList<Chapter> chapters)
        {
            return (chapters.Select(u => u.ToDTO())).ToList();
        }

        public static RelationshipDTO ToDTO(this Relationship relationship)
        {
            var tmp = new RelationshipDTO
            {
                //Origin=relationship.Origin.ToDTO(),
                //Destination=relationship.Destination.ToDTO(),
                Input = relationship.Input,
                Output = relationship.Output,
                Body = relationship.Body,
                StoryId = relationship.StoryId
            };
            return tmp;
        }

        public static List<RelationshipDTO> ToDTO(this IList<Relationship> relationships)
        {
            return (relationships.Select(u => u.ToDTO())).ToList();
        }

        public static List<PlayerDTO> ToDTO(this IList<Player> players)
        {
            return (players.Select(u => u.ToDTO())).ToList();
        }

        public static PlayerDTO ToDTO(this Player player)
        {
            var tmp = new PlayerDTO
            {
                Id = player.Id,
                DiscordId = player.DiscordId,
                CurrentChapter = player.CurrentChapter.ToDTO(),
                ChapterId = player.ChapterId,
                Ability = player.Ability,
                Endurance = player.Endurance,
            };
            return tmp;
        }
    }
}
