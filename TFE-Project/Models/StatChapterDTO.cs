using Microsoft.Extensions.Hosting;
using System;

namespace TFE_Project.Models
{
    public class StatChapterDTO
    {
        public int TotalPlayers { get; set; }
        public int NbCurrentPlayers { get; set; }
        public int ChapterId { get; set; }
        public int Quits { get; set; }
        public int? Deaths { get; set; }
    }
}
