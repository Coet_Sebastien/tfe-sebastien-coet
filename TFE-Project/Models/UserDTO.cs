﻿using System;

namespace TFE_Project.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Pseudo { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
        //public IList<StoryDTO> Creations { get; set; }
    }
}
