using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public class PlayerLog
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string DiscordId { get; set; }
        public int ChapterId { get; set; }
        public virtual Chapter Chapter { get; set; }
        public int? OldChapterId { get; set; }
        public virtual Chapter? OldChapter { get; set; }
        public int StoryId { get; set; }
        public virtual Story Story { get; set; }
        public int EnduranceLeft { get; set; }
        public string Action { get; set; }
        public DateTime Timestamp { get; set; } = DateTime.Now;
    }
}
