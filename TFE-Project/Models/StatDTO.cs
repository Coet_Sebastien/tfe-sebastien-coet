using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;

namespace TFE_Project.Models
{
    public class StatDTO
    {
        public List<StatChapterDTO> StatChaptersDTO { get; set; } = new List<StatChapterDTO>();
        public List<StatRelDTO> StatRelsDTO { get; set; } = new List<StatRelDTO>();
    }
}
