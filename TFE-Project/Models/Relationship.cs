﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public class Relationship
    {
        public int Input { get; set; }
        public virtual Chapter Origin { get; set; }
        public int Output { get; set; }
        public virtual Chapter Destination { get; set; }
        [Required]
        public string Body { get; set; }
        public int StoryId { get; set; }
        public virtual Story Story { get; set; }
    }
}
