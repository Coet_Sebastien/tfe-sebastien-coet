﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace TFE_Project.Models
{
    public class TFEContext : DbContext
    {
        public TFEContext(DbContextOptions<TFEContext> options)
        : base(options)
        {
        }

        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Story> Stories { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<Combat> Combats { get; set; }
        public DbSet<Scenario> Scenarios { get; set; }
        public DbSet<Relationship> Relationships { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<PlayerLog> PlayerLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Chapter>()
                .HasDiscriminator(c => c.Type)
                .HasValue<Scenario>("Scenario")
                .HasValue<Combat>("Combat")
                .HasValue<EnduranceChangeEvent>("EnduranceChangeEvent");

            modelBuilder.Entity<User>()
                .HasMany<Story>()
                .WithOne(e => e.Author)
                .OnDelete(DeleteBehavior.SetNull)
                .IsRequired(false);
            modelBuilder.Entity<User>()
                .HasIndex(u => new { u.Pseudo, u.Email })
                .IsUnique(true);
            modelBuilder.Entity<Player>()
                .HasIndex(u => new { u.DiscordId })
                .IsUnique(true);
            modelBuilder.Entity<Chapter>()
                .HasOne(c => c.Story)
                .WithMany(p => p.Chapters)
                .HasForeignKey(c => c.StoryId);
            modelBuilder.Entity<Relationship>().HasKey(f => new { f.Input, f.Output });
            modelBuilder.Entity<Relationship>()
                .HasOne(r=>r.Origin)
                .WithMany(c=>c.OutgoingRelationships)
                .HasForeignKey(r=>r.Input);
            modelBuilder.Entity<Relationship>()
                .HasOne(r=>r.Destination)
                .WithMany(c=>c.IncomingRelationships)
                .HasForeignKey(r=>r.Output);
            modelBuilder.Entity<Story>()
                .HasMany(s => s.Relationships)
                .WithOne(p => p.Story);
            //logs    
            modelBuilder.Entity<Story>()
                .HasMany<PlayerLog>()
                .WithOne(e => e.Story)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Chapter>()
                .HasMany<PlayerLog>()
                .WithOne(e => e.Chapter)
                .HasForeignKey(c => c.ChapterId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Chapter>()
                .HasMany<PlayerLog>()
                .WithOne(e => e.OldChapter)
                .HasForeignKey(c => c.OldChapterId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired(false);
        }
    }
}
