using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public class Combat : Chapter
    {
        public int Ability { get; set; }
        public int Endurance { get; set; }
        //to check if id and relationship are coherent
        public int VictoryId {get;set;}
        public override string Type {get;set;} = "Combat";
    }
}
