﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using Azure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace TFE_Project.Models
{
    public class PlayerDTO
    {
        public int Id { get; set; }
        public string DiscordId { get; set; }
        public int ChapterId { get; set; }
        public virtual ChapterDTO? CurrentChapter { get; set; }
        public int Ability { get; set; }
        public int Endurance { get; set; }
    }
}
