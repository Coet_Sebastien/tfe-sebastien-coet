﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;

namespace TFE_Project.Models
{
    public class StoryDTO
    {
        public int StoryId { get; set; }
        public UserDTO? Author { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public DateTime Timestamp { get; set; }
        public List<ChapterDTO> Chapters { get; set; } = new List<ChapterDTO>();
        public List<RelationshipDTO> Relationships { get; set; } = new List<RelationshipDTO>();
        public int BeginningChapterId { get; set; }
        public int? TotalPlayers { get; set; }
    }
}
