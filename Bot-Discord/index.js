const fs = require('node:fs');
const path = require('node:path');
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');
const { token, baseURL } = require('./config.json');

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent] });

async function checkStatus(response) {
	if (response.ok) {
		return response;
	} else {
		//console.log(response);
		throw new Error("");
	}
}

client.commands = new Collection();
const foldersPath = path.join(__dirname, 'commands');
const commandFolders = fs.readdirSync(foldersPath);

for (const folder of commandFolders) {
	const commandsPath = path.join(foldersPath, folder);
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));
	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file);
		const command = require(filePath);
		if ('data' in command && 'execute' in command) {
			client.commands.set(command.data.name, command);
		} else {
			console.log(`[WARNING] The command at ${filePath} is missing a required "data" or "execute" property.`);
		}
	}
}

global.tokenDB;

async function logOn() {
	var tmp=await fetch(baseURL+'api/users/authenticate', {
		method: 'POST',
		body: JSON.stringify({ "email":"bot@bot.com", "password":"bot", "pseudo": "bot" }),
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(checkStatus).then((response) => response.json()).then((data)=>{console.log("bot connected to api");global.tokenDB=data.token;}).catch((error) => {
		//throw new Error("");
		console.log("error authentification");
	})
}

client.once(Events.ClientReady, () => {
	console.log('Ready!');
	try{
		logOn();
	}catch{
		console.log("error authentification");
	}
});

client.on(Events.InteractionCreate, async interaction => {
	if (!interaction.isChatInputCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	if (interaction.commandName == "begin"
		|| interaction.commandName == "moveto"
		|| interaction.commandName == "continue"
		|| interaction.commandName == "quit") {
		await interaction.deferReply();
		await command.execute(interaction);
	} else {
		try {
			await command.execute(interaction);
		} catch (error) {
			console.error(error);
			if (interaction.replied) {
				await interaction.followUp({ content: 'There was an error while executing this command!', ephemeral: true });
			} else {
				await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
			}
		}
	}
});

client.login(token);