const { SlashCommandBuilder } = require('discord.js');
const { baseURL } = require('../../config.json');


function checkStatus(response) {
    if (response.ok) {
        return response;
    } else {
        throw new Error();
    }
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('quit')
        .setDescription('quit the current story'),
    async execute(interaction) {
        const tokenDB=global.tokenDB;
        const user = interaction.user.id;
        fetch(baseURL+"api/players/" + user, {
            method: "DELETE",
            headers: {
                'Authorization': `Bearer ${tokenDB}`
            }
        }).then(interaction.editReply("Deleted"))
    },
};
