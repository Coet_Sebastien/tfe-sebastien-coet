const { SlashCommandBuilder } = require('discord.js');
const { baseURL } = require('../../config.json');

const damageTable =
    [
        [999, 999, 8, 6, 6, 5, 5, 5, 4, 4, 4, 3, 3],    // dieRoll == 1
        [999, 8, 7, 6, 5, 5, 4, 4, 3, 3, 3, 3, 2],      // dieRoll == 2
        [8, 7, 6, 5, 5, 4, 4, 3, 3, 3, 2, 2, 2],        // dieRoll == 3
        [8, 7, 6, 5, 4, 4, 3, 3, 2, 2, 2, 2, 2],        // dieRoll == 4
        [7, 6, 5, 4, 4, 3, 2, 2, 2, 2, 2, 2, 1],        // dieRoll == 5
        [6, 6, 5, 4, 3, 2, 2, 2, 2, 1, 1, 1, 1],        // dieRoll == 6
        [5, 5, 4, 3, 2, 2, 1, 1, 1, 0, 0, 0, 0],        // dieRoll == 7
        [4, 4, 3, 2, 1, 1, 0, 0, 0, 0, 0, 0, 0],        // dieRoll == 8
        [3, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],        // dieRoll == 9
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]         // dieRoll == 10
    ];


function checkStatus(response) {
    if (response.ok) {
        return response;
    } else {
        console.log(response)
        throw new Error();
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

async function combat(playerCurrentEndurance, opponentCurrentEndurance, combatRatio, interaction, intro) {
    //console.log("Entering combat");
    var toSend = intro + '\n' + '\n';
    while (playerCurrentEndurance > 0 && opponentCurrentEndurance > 0) {
        //d10
        var luck = getRandomInt(10) + 1;
        toSend += 'Rolled a ' + luck;
        var e = combatRatio + luck;
        if (e > 10) {
            e = 10 + (e - 10) * 2;
        }
        e += 2;
        if (e < 0) {
            e = 0;
        }
        var lw = damageTable.at(luck - 1).at(6 + combatRatio);
        playerCurrentEndurance -= lw;
        opponentCurrentEndurance -= e;
        toSend += '\n' + 'Player attacks for ' + e + " damage leaving opponent with " + opponentCurrentEndurance + " endurance points." + '\n'
        toSend += 'You take ' + lw + " damage leaving you with " + playerCurrentEndurance + " endurance points." + '\n' + '\n'
    }
    //console.log("player HP:" + playerCurrentEndurance);
    if (playerCurrentEndurance < 1) {
        toSend += "You died";
    } else {
        toSend += "You win";
    }
    return { playerCurrentEndurance, toSend };
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('moveto')
        .setDescription('move to the selected chapter')
        .addNumberOption(option =>
            option.setName('choice')
                .setDescription('id of the choice')
                .setRequired(true)),
    async execute(interaction) {
        const tokenDB = global.tokenDB;
        const choice = interaction.options.getNumber('choice');
        const user = interaction.user.id;
        var ok = await fetch(baseURL+"api/players/movePlayer/" + user +"/"+ choice, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${tokenDB}`
            }
        }).then(checkStatus).then((response) => response.json()).then(async (data) => {
            //console.log(data);
            switch (data.currentChapter.type) {
                case 'Scenario':
                    //console.log("Scenario");
                    var textToSend = (data.currentChapter.body + '\n');
                    if (data.currentChapter.outgoingRelationships.length === 0) {
                        textToSend += '\n' + "THE END";
                        fetch(baseURL+"api/players/" + user, {
                            method: "DELETE",
                            headers: {
                                'Authorization': `Bearer ${tokenDB}`
                            }
                        })
                    }
                    var counter=1;
                    data.currentChapter.outgoingRelationships.forEach(or => {
                        textToSend += '\n' + counter + ") " + or.body;
                        counter++;
                    });
                    interaction.editReply(textToSend);
                    break;
                case 'Combat':
                    //console.log("Combat");
                    var intro = ("Entering combat:" + '\n' + "Opponent Combat Skill: " + data.currentChapter.ability + '      ' + "Opponent Endurance Points: " + data.currentChapter.endurance + '\n' + "Player Combat Skill: " + data.ability + '        ' + "Player Endurance Points: " + data.endurance);
                    //console.log(textToSend);
                    //combat calculations
                    var combatRatio = Math.round(parseFloat((data.ability - data.currentChapter.ability) / 2));
                    if (combatRatio > 6)
                        combatRatio = 6;
                    if (combatRatio < -6)
                        combatRatio = -6;
                    var playerCurrentEndurance = Number(data.endurance);
                    var opponentCurrentEndurance = Number(data.currentChapter.endurance);
                    await combat(playerCurrentEndurance, opponentCurrentEndurance, combatRatio, interaction, intro).then(async ({ playerCurrentEndurance, toSend }) => {
                        //console.log(playerCurrentEndurance);
                        interaction.editReply(toSend).then(async () => {
                            if (playerCurrentEndurance < 1) {
                                //lose
                                fetch(baseURL+"api/players/" + user, {
                                    method: "DELETE",
                                    headers: {
                                        'Authorization': `Bearer ${tokenDB}`
                                    }
                                }).catch((err) => {
                                    console.log(err);
                                    interaction.channel.send('Error');
                                });
                            } else {
                                await fetch(baseURL+"api/players/" + user, {
                                    method: "PUT",
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Authorization': `Bearer ${tokenDB}`
                                    },
                                    body: JSON.stringify({
                                        discordId: user,
                                        chapterId: data.currentChapter.victoryId,
                                        endurance: playerCurrentEndurance,
                                    })
                                }).then(checkStatus).then((response) => response.json()).then(async (dataCombat) => {
                                    var textToSend2 = (dataCombat.currentChapter.body + '\n');
                                    if (dataCombat.currentChapter.outgoingRelationships.length === 0) {
                                        textToSend2 += '\n' + "THE END";
                                        fetch(baseURL+"api/players/" + user, {
                                            method: "DELETE",
                                            headers: {
                                                'Authorization': `Bearer ${tokenDB}`
                                            }
                                        }).catch((err) => {
                                            console.log(err);
                                            interaction.channel.send('Error deleting');
                                        });
                                    }
                                    var counter=1;
                                    dataCombat.currentChapter.outgoingRelationships.forEach(or => {
                                        textToSend2 += '\n' + counter + ") " + or.body;
                                        counter++;
                                    });
                                    interaction.followUp(textToSend2);
                                }).catch((err) => {
                                    console.log(err);
                                    interaction.channel.send('Error victory');
                                });
                            }
                        });

                    });
                    break;
                case 'EnduranceChangeEvent':
                    var playerCurrentEndurance = Number(data.endurance);
                    var mod = Number(data.currentChapter.enduranceModifier);
                    var textToSend = "";
                    playerCurrentEndurance+=mod;
                    if(mod>0){
                        if(playerCurrentEndurance>25){
                            playerCurrentEndurance=25;
                        }
                        textToSend+= "You regain "+mod+" endurance points. You're back at "+playerCurrentEndurance+" EP.";
                    }
                    if(mod===0){
                        textToSend+= "You don't feel a change.";
                    }
                    if(mod<0){
                        var opp=0-mod;
                        textToSend+= "You lose "+opp+" endurance points. You're at "+playerCurrentEndurance+" EP.";
                    }
                    //console.log("HP="+playerCurrentEndurance);
                    interaction.editReply(textToSend).then(async () => {
                        if (playerCurrentEndurance < 1) {
                            //lose
                            fetch(baseURL+"api/players/" + user, {
                                method: "DELETE",
                                headers: {
                                    'Authorization': `Bearer ${tokenDB}`
                                }
                            }).catch((err) => {
                                console.log(err);
                                interaction.channel.send('Error');
                            });
                        } else {
                            await fetch(baseURL+"api/players/" + user, {
                                method: "PUT",
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${tokenDB}`
                                },
                                body: JSON.stringify({
                                    discordId: user,
                                    chapterId: data.currentChapter.victoryId,
                                    endurance: playerCurrentEndurance,
                                })
                            }).then(checkStatus).then((response) => response.json()).then(async (dataCombat) => {
                                var textToSend2 = (dataCombat.currentChapter.body + '\n');
                                if (dataCombat.currentChapter.outgoingRelationships.length === 0) {
                                    textToSend2 += '\n' + "THE END";
                                    fetch(baseURL+"api/players/" + user, {
                                        method: "DELETE",
                                        headers: {
                                            'Authorization': `Bearer ${tokenDB}`
                                        }
                                    }).catch((err) => {
                                        console.log(err);
                                        interaction.channel.send('Error');
                                    });
                                }
                                var counter=1;
                                dataCombat.currentChapter.outgoingRelationships.forEach(or => {
                                    textToSend2 += '\n' + counter + ") " + or.body;
                                    counter++;
                                });
                                interaction.followUp(textToSend2);
                            }).catch((err) => {
                                console.log(err);
                                interaction.channel.send('Wrong id');
                            });
                        }
                    });;
                    break;
                default:
                    interaction.editReply("Error");
                    break;
            }

        }).catch((err) => {
            //console.log(err);
            interaction.editReply('Wrong id');
        });
    },
};