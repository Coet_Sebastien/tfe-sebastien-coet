const { SlashCommandBuilder } = require('discord.js');
const { baseURL } = require('../../config.json');


async function checkStatus(response) {
	if (response.ok) {
		return response;
	} else {
		//console.log(response);
		throw new Error("");
	}
}

module.exports = {
	data: new SlashCommandBuilder()
		.setName('begin')
		.setDescription('begin a story')
		.addNumberOption(option =>
			option.setName('storyid')
				.setDescription('id of the story to begin')
				.setRequired(true)),
	async execute(interaction) {
		const tokenDB=global.tokenDB;
		const user = interaction.user.id;
		const storyId = interaction.options.getNumber('storyid');
		await fetch(baseURL+'api/stories/' + storyId, {
			headers: {
                'Authorization': `Bearer ${tokenDB}`
            }
		}).then(checkStatus).then((response) => response.json()).then((result) => {
			const filter = m => {
				//console.log(m);
				return m.content.includes('Y') && m.author.id === user;
			};
			//console.log(file);
			interaction.editReply({ content: `Do you want to begin the story \"${result.title}\" ? Type "Y" to accept`, fetchReply: true })
				.then(() => {
					interaction.channel.awaitMessages({ filter, max: 1, maxProcessed: 1, time: 30000, errors: ['time'] })
						.then(async (collected) => {
							// console.log(collected.first().author.id, file);
							// console.log(JSON.stringify({
							// 	discordId: collected.first().author.id,
							// 	chapterId: file.beginningChapterId
							// }))
							var ok = await fetch(baseURL+"api/players", {
								method: "POST",
								headers: {
									'Content-Type': 'application/json',
									'Authorization': `Bearer ${tokenDB}`
								},
								body: JSON.stringify({
									discordId: collected.first().author.id,
									chapterId: result.beginningChapterId
								})
							}).then(checkStatus).then((response) => response.json()).then((data) => {
								//console.log(data);
								var textToSend = (data.currentChapter.body + '\n')
								if (data.currentChapter.outgoingRelationships.length === 0) {
									textToSend += '\n' + "THE END";
									fetch(baseURL+"api/players/" + user, {
										method: "DELETE",
										headers: {
											'Authorization': `Bearer ${tokenDB}`
										}
									})
								} else {
									var counter = 1;
									data.currentChapter.outgoingRelationships.forEach(or => {
										textToSend += '\n' + counter + ") " + or.body;
										counter++;
									});
									textToSend += ('\n' + '\n' + "Use /moveto followed by the id of your choice");
								}
								interaction.followUp(textToSend);
							}).catch((err) => {
								console.log(err);
								interaction.followUp('You already have another story in progress');
							});
						}).catch(() => {
							interaction.followUp('Cancelled');
						})
				});
		}
		).catch((err) => {
			console.log(err);
			interaction.followUp('Wrong id');
		});;

	},
};
