const { SlashCommandBuilder } = require('discord.js');
const { baseURL } = require('../../config.json');


function checkStatus(response) {
    if (response.ok) {
        return response;
    } else {
        throw new Error();
    }
}

module.exports = {
    data: new SlashCommandBuilder()
        .setName('continue')
        .setDescription('continue a story'),
    async execute(interaction) {
        const tokenDB = global.tokenDB;
        const user = interaction.user.id;
        const result = await fetch(baseURL+"api/players/" + user, {
            headers: {
                'Authorization': `Bearer ${tokenDB}`
            }
        }).then((response) => response.json()).then((data) => {
            //console.log(data);
            if (data.currentChapter.type === 'Scenario') {
                var textToSend = (data.currentChapter.body + '\n')
                if (data.currentChapter.outgoingRelationships.length === 0) {
                    textToSend += '\n' + "THE END";
                    fetch(baseURL+"api/players/" + user, {
                        method: "DELETE",
                        headers: {
                            'Authorization': `Bearer ${tokenDB}`
                        }
                    })
                } else {
                    var counter = 1;
                    data.currentChapter.outgoingRelationships.forEach(or => {
                        textToSend += '\n' + counter + ") " + or.body;
                        counter++;
                    });
                    textToSend += ('\n' + '\n' + "Use /moveto followed by the id of your choice");
                }
                interaction.editReply(textToSend);
            }else{
                interaction.editReply("Finish your action first");
            }
        }).catch((err) => {
            interaction.editReply('No story in progress');
        });
    },
};
