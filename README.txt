For local run:
- in TFE-Project/appsettings.json and appsettings.Development.json: "tfe-mysql": "Server=localhost;Database=tfe-db;Uid=root;Pwd=root"
- in Bot-Discord/config.json: "baseURL":"http://localhost:5000/"
- comment in TFE-Project/Program.cs: db.Database.Migrate(); // apply all migrations (if you don't already have migrations)

For Docker run:
- in TFE-Project/appsettings.json and appsettings.Development.json: "tfe-mysql": "Server=db;Port=3306;Database=tfe-db;Uid=root;Pwd=root"
- in Bot-Discord/config.json: "baseURL":"http://api:80/"
- uncomment in TFE-Project/Program.cs: db.Database.Migrate(); // apply all migrations

For the professors:
- Online site at: http://tfe-sebastien-coet.duckdns.org/
- login and password for them: profs@test.com / profs